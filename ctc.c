/*
 * ctc.c
 *
 *  Created on: 2014-06-06
 *      Author: s4sehgal
 */
#ifdef OLDCTC
#include "pid.h"
#include "ctc.h"
#include "CarControls.h"
#include "controlUtil.h"
#include "mat.h"



int ctcModel(void * latestData){
	static int i = 0;
	static double y = 0;

	con_task_t * ctcData = (con_task_t *)latestData;
	double pos = log2distX(ctcData->adcTm);
	printf("%lf, %lf \n", ctcData->adcTm, pos);

	y = pid_update_fY(0, pos);			//y: motor

	if (y >= 2.5) 						//Safe saturation limits, 14.86+Y Or 14.86-0, where 14.86 is idle
		y = 2.5;
	if (y <= 0)
		y = 0;

	ctcData->pwmTm = 14.86 + y;

	if (i <= 2)
	{
		ctcData->pwmTm = 14.86;
	}
	i++;
}

double pid_update_fY(double sp,double pv)
{
	double absErr = sp-pv;
	static double absErrL = 0;
	static double intErr = 0;
	static double difErr = 0;
	double pidE = 0;

	intErr = intErr + (absErr * DELTAT);
	difErr = (absErr - absErrL)/ DELTAT;

	pidE = KP_Y * absErr + KI_Y * intErr + KD_Y * difErr;

	//Integrator anti-windup
	if (absD(intErr) >= 50)
		intErr = 50;

	absErrL = absErr;

	return pidE;
}


	if (ctcData->ESTOP == 1)
	{	//Emergency Stop
		//ctcData->speedRealN = 0;
		ctcData->pwmTm = 0;
	}
	else;

	if (ctcData->lane == 0)								// 0: left, 1: Right
	{
		//x = pid_update_fX(LEFTLOC, ctcData->deltaX);			//x: steer
		x = pid_update_fX(0, ctcData->deltaX);			//x: steer
		y = pid_update_fY(0, ctcData->deltaY);			//y: motor
	}
	else
	{
		//x = pid_update_fX(XOFFSET, ctcData->deltaX);	//x: steer
		x = pid_update_fX((LEFTLOC+XOFFSET), ctcData->deltaX);	//x: steer
		y = pid_update_fY(0, ctcData->deltaY);			//y: motor
	}

	printf( "%lf ", ctcData->deltaX );
	printf( "%lf ", ctcData->deltaY );

	//Safe saturation limits, 15+3 or 15-3, where 15 is straight
		if (x >= 4.0)
			x = 4.0;
		if (x <= -4.0)
			x = -4.0;


	//Safe saturation limits, 15+5 0r 15-0, where 15 is idle
		if (y >= 5.0)
			y = 5.0;
		if (y <= -5.0)
			y = -5.0;


	//Fill LPF window
		xLLLL = xLLL;
		xLLL = xLL;
		xLL = xL;
		xL = x;
		//var = 15900000 + (20000 *((x + xL + xLL + xLLL + xLLLL)/5.0));

	//Fill LPF window
		yLLLL = yLLL;
		yLLL = yLL;
		yLL = yL;
		yL = y;


 	//LPF
		//ctcData->pwmSt = (15.9 + ((x + xL + xLL + xLLL + xLLLL)/5.0));
		// old - 14.43
		ctcData->pwmSt = 14.53 - x;
		printf("%lf ", 14.53 - x);
		//ctcData->pwmSt = (var / 1000000.0);

	//LPF
		//ctcData->pwmTm = (14.86 + ((y + yL + yLL + yLLL + yLLLL)/5.0));
		ctcData->pwmTm = 14.86 + y;
		printf("%lf \n", 14.86 + y);
//		if( y > 0 ) {
//			ctcData->pwmTm = 14.86 + y;
//			yL = y;
//		} else {
//			ctcData->pwmTm = 14.86 + yL;
//		}
		//ctcData->pwmTm = 15.79;
		//ctcData->pwmTm = 14.86;

	//printout scaledown
	if (i <= 2)
	{
		//printf("Steer:%lf, Motor:%lf \n",ctcData->pwmSt, ctcData->pwmTm);
		//printf("delX:%lf, Steer:%lf \n",ctcData->deltaX, ctcData->pwmSt);
		//printf("%lf,%lf \n",(ctcData->deltaX-51.5), (ctcData->pwmSt - 15.9));
		//printf("%lf,%lf \n",(ctcData->deltaX), (ctcData->pwmSt - 15.9));
		ctcData->pwmTm = 14.86;
		//i = 0;
	}
	i++;



double pid_update_fX(double sp,double pv)
{
	static double absErr = 0.0;
	static double absErrL = 0;
	static double intErr = 0;
	static double difErr = 0;
	double pidE = 0;

	absErr = sp-pv;
	intErr = intErr + (absErr * DELTAT);
	difErr = (absErr - absErrL)/ DELTAT;

	pidE = KP_X * absErr + KI_X * intErr + KD_X * difErr;
	if  (intErr > 0.08)     //integral anti-windup
		intErr = 0.08;
	if (intErr < -0.08)
		intErr = -0.08;

	absErrL = absErr;
	return pidE;
}
#endif
