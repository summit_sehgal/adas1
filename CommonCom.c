/*
 * CommonCom.c
 *
 *  Created on: 2014-03-24
 *      Author: s4sehgal
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
//#include <string.h>
//#include <sys/neutrino.h>
//#include <sys/select.h>


void SerialSetup_Stream(	int* fd,
							int baudrate,
							int dataBits,
							int parity,
							int stopBits )
{
	//Serial Port Setting Variables
	struct termios oldtio, newtio;
	int BAUD, DATABITS, PARITY, PARITYON, STOPBITS;
	//Setup and Store Serial Port Settings
	tcgetattr(*fd,&oldtio);
	newtio = oldtio; //YR this is important!
	cfsetispeed(&newtio,baudrate);
	cfsetospeed(&newtio,baudrate);

	/* baudrate settings */
	    switch (baudrate) {
	    case 38400:
	        BAUD = B38400;
	        break;
	    case 19200:
	        BAUD  = B19200;
	        break;
	    case 9600:
	        BAUD  = B9600;
	        break;
	    default:
	        BAUD = B38400;
	    }

	    /* databits settings */
	    switch (dataBits) {
	    case 8:
	        DATABITS = CS8;
	        break;
	    case 7:
	        DATABITS = CS7;
	        break;
	    default:
	        DATABITS = CS8;
	    }

	    /* stop bits */
	    switch (stopBits) {
	    case 1:
	        STOPBITS = 0;
	        break;
	    case 2:
	        STOPBITS = CSTOPB;
	        break;
	    default:
	        STOPBITS = 0;

	    }

	    /* parity */
	    switch (parity) {
	    case 0: /* no parity */
	        PARITYON = 0;
	        PARITY = 0;
	        break;
	    case 1: /* odd parity */
	        PARITYON = PARENB;
	        PARITY = PARODD;
	        break;
	    case 2: /* event parity */
	        PARITYON = PARENB;
	        PARITY = 0;
	        break;
	    default: /* none */
	        PARITYON = 0;
	        PARITY = 0;
	    }

	newtio.c_iflag &= ~(IGNBRK | BRKINT | ICRNL |
	                    INLCR | PARMRK | INPCK | ISTRIP | IXON | IXOFF);
	newtio.c_oflag = 0;
	newtio.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);
	newtio.c_cflag = BAUD | DATABITS | STOPBITS | PARITYON | PARITY | CLOCAL | CREAD;
	newtio.c_cc[VMIN]  = 1;
	newtio.c_cc[VTIME] = 0;
	tcsetattr(*fd, TCSAFLUSH, &newtio);
}
