/*
 * stanley_y.c
 *
 * Code generation for model "stanley_y".
 *
 * Model version              : 1.9
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Thu Nov 27 16:23:27 2014
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */
#include "stanley_y.h"
#include "stanley_y_private.h"

/* Model step function */
void stanley_y_step(RT_MODEL_stanley_y_T *const stanley_y_M)
{
  P_stanley_y_T *stanley_y_P = ((P_stanley_y_T *)
    stanley_y_M->ModelData.defaultParam);
  ExtU_stanley_y_T *stanley_y_U = (ExtU_stanley_y_T *)
    stanley_y_M->ModelData.inputs;
  ExtY_stanley_y_T *stanley_y_Y = (ExtY_stanley_y_T *)
    stanley_y_M->ModelData.outputs;

  /* Outport: '<Root>/Out1' incorporates:
   *  Constant: '<Root>/ScalingConst'
   *  Constant: '<Root>/TuningConst'
   *  Inport: '<Root>/In1'
   *  Product: '<Root>/Product'
   *  Product: '<Root>/Product3'
   *  Trigonometry: '<Root>/Trigonometric Function'
   */
  stanley_y_Y->Out1 = atan(0.1 * stanley_y_U->In1) *
    3.6;
  //printf("CONST: %lf\n", stanley_y_P->ScalingConst_Value);

#ifdef DUMMY
  /* Matfile logging */
  rt_UpdateTXYLogVars(stanley_y_M->rtwLogInfo, (&stanley_y_M->Timing.taskTime0));

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.2s, 0.0s] */
    if ((rtmGetTFinal(stanley_y_M)!=-1) &&
        !((rtmGetTFinal(stanley_y_M)-stanley_y_M->Timing.taskTime0) >
          stanley_y_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(stanley_y_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++stanley_y_M->Timing.clockTick0)) {
    ++stanley_y_M->Timing.clockTickH0;
  }

  stanley_y_M->Timing.taskTime0 = stanley_y_M->Timing.clockTick0 *
    stanley_y_M->Timing.stepSize0 + stanley_y_M->Timing.clockTickH0 *
    stanley_y_M->Timing.stepSize0 * 4294967296.0;
#endif
}

/* Model initialize function */
void stanley_y_initialize(RT_MODEL_stanley_y_T *const stanley_y_M)
{
  UNUSED_PARAMETER(stanley_y_M);
}

/* Model terminate function */
void stanley_y_terminate(RT_MODEL_stanley_y_T * stanley_y_M)
{
  /* model code */
  {
    real_T *ptr = (real_T *) stanley_y_M->ModelData.inputs;
    rt_FREE(ptr);
  }

  {
    real_T *ptr = (real_T *) stanley_y_M->ModelData.outputs;
    rt_FREE(ptr);
  }

  if (stanley_y_M->ModelData.paramIsMalloced) {
    rt_FREE(stanley_y_M->ModelData.defaultParam);
  }

  {
    void *xptr = (void *) rtliGetLogXSignalPtrs(stanley_y_M->rtwLogInfo);
    void *yptr = (void *) rtliGetLogYSignalPtrs(stanley_y_M->rtwLogInfo);
    rt_FREE(xptr);
    rt_FREE(yptr);
  }

  rt_FREE(stanley_y_M->rtwLogInfo);
  rt_FREE(stanley_y_M);
}

/* Model data allocation function */
RT_MODEL_stanley_y_T *stanley_y(void)
{
  RT_MODEL_stanley_y_T *stanley_y_M;
  stanley_y_M = (RT_MODEL_stanley_y_T *) malloc(sizeof(RT_MODEL_stanley_y_T));
  if (stanley_y_M == NULL) {
    return NULL;
  }

  (void) memset((char *)stanley_y_M, 0,
                sizeof(RT_MODEL_stanley_y_T));

  /* Setup for data logging */
  {
    RTWLogInfo *rt_DataLoggingInfo = (RTWLogInfo *) malloc(sizeof(RTWLogInfo));
    //rt_VALIDATE_MEMORY(stanley_y_M,rt_DataLoggingInfo);
    stanley_y_M->rtwLogInfo = rt_DataLoggingInfo;
  }

  /* Setup for data logging */
  {
    rtliSetLogXSignalInfo(stanley_y_M->rtwLogInfo, (NULL));
    rtliSetLogXSignalPtrs(stanley_y_M->rtwLogInfo, (NULL));

    /*
     * Set pointers to the data and signal info for each output
     */
    {
      void **rt_LoggedOutputSignalPtrs = (void **)malloc(1*sizeof(void*));
      //rt_VALIDATE_MEMORY(stanley_y_M,rt_LoggedOutputSignalPtrs);
      rtliSetLogYSignalPtrs(stanley_y_M->rtwLogInfo, ((LogSignalPtrsType)
        rt_LoggedOutputSignalPtrs));
    }

    {
    }
  }

  /* parameters */
  {
    P_stanley_y_T *p;
    static int_T pSeen = FALSE;

    /* only malloc on multiple model instantiation */
    if (pSeen == TRUE ) {
      p = (P_stanley_y_T *) malloc(sizeof(P_stanley_y_T));
      //rt_VALIDATE_MEMORY(stanley_y_M,p);
      (void) memcpy(p, &stanley_y_P,
                    sizeof(P_stanley_y_T));
      stanley_y_M->ModelData.paramIsMalloced = (TRUE);
    } else {
      p = &stanley_y_P;
      stanley_y_M->ModelData.paramIsMalloced = (FALSE);
      pSeen = TRUE;
    }

    stanley_y_M->ModelData.defaultParam = (p);
  }

  /* external inputs */
  {
    ExtU_stanley_y_T *stanley_y_U = (ExtU_stanley_y_T *) malloc(sizeof
      (ExtU_stanley_y_T));
    //rt_VALIDATE_MEMORY(stanley_y_M,stanley_y_U);
    stanley_y_M->ModelData.inputs = (((ExtU_stanley_y_T *) stanley_y_U));
  }

  /* external outputs */
  {
    ExtY_stanley_y_T *stanley_y_Y = (ExtY_stanley_y_T *) malloc(sizeof
      (ExtY_stanley_y_T));
    //rt_VALIDATE_MEMORY(stanley_y_M,stanley_y_Y);
    stanley_y_M->ModelData.outputs = (stanley_y_Y);
  }

  {
    ExtU_stanley_y_T *stanley_y_U = (ExtU_stanley_y_T *)
      stanley_y_M->ModelData.inputs;
    ExtY_stanley_y_T *stanley_y_Y = (ExtY_stanley_y_T *)
      stanley_y_M->ModelData.outputs;

    /* initialize non-finites */
    rt_InitInfAndNaN(sizeof(real_T));
    rtmSetTFinal(stanley_y_M, 10.0);
    stanley_y_M->Timing.stepSize0 = 0.2;

    /* Setup for data logging */
    {
      rtliSetLogT(stanley_y_M->rtwLogInfo, "tout");
      rtliSetLogX(stanley_y_M->rtwLogInfo, "");
      rtliSetLogXFinal(stanley_y_M->rtwLogInfo, "");
      rtliSetSigLog(stanley_y_M->rtwLogInfo, "");
      rtliSetLogVarNameModifier(stanley_y_M->rtwLogInfo, "rt_");
      rtliSetLogFormat(stanley_y_M->rtwLogInfo, 0);
      rtliSetLogMaxRows(stanley_y_M->rtwLogInfo, 1000);
      rtliSetLogDecimation(stanley_y_M->rtwLogInfo, 1);

      /*
       * Set pointers to the data and signal info for each output
       */
      {
        ((void **) rtliGetLogYSignalPtrs(stanley_y_M->rtwLogInfo))[0] =
          &stanley_y_Y->Out1;
      }

      {
        static int_T rt_LoggedOutputWidths[] = {
          1
        };

        static int_T rt_LoggedOutputNumDimensions[] = {
          1
        };

        static int_T rt_LoggedOutputDimensions[] = {
          1
        };

        static boolean_T rt_LoggedOutputIsVarDims[] = {
          0
        };

        static void* rt_LoggedCurrentSignalDimensions[] = {
          (NULL)
        };

        static int_T rt_LoggedCurrentSignalDimensionsSize[] = {
          4
        };

        static BuiltInDTypeId rt_LoggedOutputDataTypeIds[] = {
          SS_DOUBLE
        };

        static int_T rt_LoggedOutputComplexSignals[] = {
          0
        };

        static const char_T *rt_LoggedOutputLabels[] = {
          "" };

        static const char_T *rt_LoggedOutputBlockNames[] = {
          "stanley_y/Out1" };

        static RTWLogDataTypeConvert rt_RTWLogDataTypeConvert[] = {
          { 0, SS_DOUBLE, SS_DOUBLE, 0, 0, 0, 1.0, 0, 0.0 }
        };

        static RTWLogSignalInfo rt_LoggedOutputSignalInfo[] = {
          {
            1,
            rt_LoggedOutputWidths,
            rt_LoggedOutputNumDimensions,
            rt_LoggedOutputDimensions,
            rt_LoggedOutputIsVarDims,
            rt_LoggedCurrentSignalDimensions,
            rt_LoggedCurrentSignalDimensionsSize,
            rt_LoggedOutputDataTypeIds,
            rt_LoggedOutputComplexSignals,
            (NULL),

            { rt_LoggedOutputLabels },
            (NULL),
            (NULL),
            (NULL),

            { rt_LoggedOutputBlockNames },

            { (NULL) },
            (NULL),
            rt_RTWLogDataTypeConvert
          }
        };

        rtliSetLogYSignalInfo(stanley_y_M->rtwLogInfo, rt_LoggedOutputSignalInfo);

        /* set currSigDims field */
        rt_LoggedCurrentSignalDimensions[0] = &rt_LoggedOutputWidths[0];
      }

      rtliSetLogY(stanley_y_M->rtwLogInfo, "yout");
    }

    stanley_y_M->Timing.stepSize = (0.2);

    /* external inputs */
    stanley_y_U->In1 = 0.0;

    /* external outputs */
    stanley_y_Y->Out1 = 0.0;
  }

  return stanley_y_M;
}
