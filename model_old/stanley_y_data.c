/*
 * stanley_y_data.c
 *
 * Code generation for model "stanley_y".
 *
 * Model version              : 1.9
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Thu Nov 27 16:23:27 2014
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */
#include "stanley_y.h"
#include "stanley_y_private.h"

/* Block parameters (auto storage) */
P_stanley_y_T stanley_y_P = {
  3.6,                                 /* Expression: 3.6
                                        * Referenced by: '<Root>/ScalingConst'
                                        */
  0.1                                  /* Expression: 0.1
                                        * Referenced by: '<Root>/TuningConst'
                                        */
};
