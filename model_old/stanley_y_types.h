/*
 * stanley_y_types.h
 *
 * Code generation for model "stanley_y".
 *
 * Model version              : 1.9
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Thu Nov 27 16:23:27 2014
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */
#ifndef RTW_HEADER_stanley_y_types_h_
#define RTW_HEADER_stanley_y_types_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"

/* Parameters (auto storage) */
typedef struct P_stanley_y_T_ P_stanley_y_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_stanley_y_T RT_MODEL_stanley_y_T;

#endif                                 /* RTW_HEADER_stanley_y_types_h_ */
