/*
 * File: step_ramp_codegen_types.h
 *
 * Code generated for Simulink model 'step_ramp_codegen'.
 *
 * Model version                  : 1.6
 * Simulink Coder version         : 8.5 (R2013b) 08-Aug-2013
 * C/C++ source code generated on : Fri Dec 19 18:05:37 2014
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_step_ramp_codegen_types_h_
#define RTW_HEADER_step_ramp_codegen_types_h_
#include "rtwtypes.h"

/* Parameters (auto storage) */
typedef struct P_step_ramp_codegen_T_ P_step_ramp_codegen_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_step_ramp_codegen_T RT_MODEL_step_ramp_codegen_T;

#endif                                 /* RTW_HEADER_step_ramp_codegen_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
