/*
 * File: step_ramp_codegen_data.c
 *
 * Code generated for Simulink model 'step_ramp_codegen'.
 *
 * Model version                  : 1.6
 * Simulink Coder version         : 8.5 (R2013b) 08-Aug-2013
 * C/C++ source code generated on : Fri Dec 19 18:05:37 2014
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#include "step_ramp_codegen.h"
#include "step_ramp_codegen_private.h"

/* Block parameters (auto storage) */
P_step_ramp_codegen_T step_ramp_codegen_P = {
  1.0,                                 /* Expression: P
                                        * Referenced by: '<S1>/Proportional Gain'
                                        */
  0.0,                                 /* Expression: InitialConditionForIntegrator
                                        * Referenced by: '<S1>/Integrator'
                                        */
  0.0,                                 /* Expression: D
                                        * Referenced by: '<S1>/Derivative Gain'
                                        */
  0.0,                                 /* Expression: InitialConditionForFilter
                                        * Referenced by: '<S1>/Filter'
                                        */
  100.0,                               /* Expression: N
                                        * Referenced by: '<S1>/Filter Coefficient'
                                        */
  1.0                                  /* Expression: I
                                        * Referenced by: '<S1>/Integral Gain'
                                        */
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
