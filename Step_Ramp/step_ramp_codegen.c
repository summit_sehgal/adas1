/*
 * File: step_ramp_codegen.c
 *
 * Code generated for Simulink model 'step_ramp_codegen'.
 *
 * Model version                  : 1.6
 * Simulink Coder version         : 8.5 (R2013b) 08-Aug-2013
 * C/C++ source code generated on : Fri Dec 19 18:05:37 2014
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#include "step_ramp_codegen.h"
#include "step_ramp_codegen_private.h"

/* Block signals (auto storage) */
B_step_ramp_codegen_T step_ramp_codegen_B;

/* Continuous states */
X_step_ramp_codegen_T step_ramp_codegen_X;

/* Block states (auto storage) */
DW_step_ramp_codegen_T step_ramp_codegen_DW;

/* Real-time model */
RT_MODEL_step_ramp_codegen_T step_ramp_codegen_M_;
RT_MODEL_step_ramp_codegen_T *const step_ramp_codegen_M = &step_ramp_codegen_M_;

/*
 * This function updates continuous states using the ODE3 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si , real_T arg_Error,
  real_T arg_VcOld, real_T arg_PIDOld, real_T *arg_PID)
{
  /* Solver Matrices */
  static const real_T rt_ODE3_A[3] = {
    1.0/2.0, 3.0/4.0, 1.0
  };

  static const real_T rt_ODE3_B[3][3] = {
    { 1.0/2.0, 0.0, 0.0 },

    { 0.0, 3.0/4.0, 0.0 },

    { 2.0/9.0, 1.0/3.0, 4.0/9.0 }
  };

  time_T t = rtsiGetT(si);
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE3_IntgData *id = (ODE3_IntgData *)rtsiGetSolverData(si);
  real_T *y = id->y;
  real_T *f0 = id->f[0];
  real_T *f1 = id->f[1];
  real_T *f2 = id->f[2];
  real_T hB[3];
  int_T i;
  int_T nXc = 2;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);

  /* Save the state values at time t in y, we'll use x as ynew. */
  (void) memcpy(y, x,
                (uint_T)nXc*sizeof(real_T));

  /* Assumes that rtsiSetT and ModelOutputs are up-to-date */
  /* f0 = f(t,y) */
  rtsiSetdX(si, f0);
  step_ramp_codegen_derivatives(arg_Error, arg_VcOld, arg_PIDOld, arg_PID);

  /* f(:,2) = feval(odefile, t + hA(1), y + f*hB(:,1), args(:)(*)); */
  hB[0] = h * rt_ODE3_B[0][0];
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0]);
  }

  rtsiSetT(si, t + h*rt_ODE3_A[0]);
  rtsiSetdX(si, f1);
  step_ramp_codegen_custom(arg_Error, arg_VcOld, arg_PIDOld, arg_PID);
  step_ramp_codegen_derivatives(arg_Error, arg_VcOld, arg_PIDOld, arg_PID);

  /* f(:,3) = feval(odefile, t + hA(2), y + f*hB(:,2), args(:)(*)); */
  for (i = 0; i <= 1; i++) {
    hB[i] = h * rt_ODE3_B[1][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1]);
  }

  rtsiSetT(si, t + h*rt_ODE3_A[1]);
  rtsiSetdX(si, f2);
  step_ramp_codegen_custom(arg_Error, arg_VcOld, arg_PIDOld, arg_PID);
  step_ramp_codegen_derivatives(arg_Error, arg_VcOld, arg_PIDOld, arg_PID);

  /* tnew = t + hA(3);
     ynew = y + f*hB(:,3); */
  for (i = 0; i <= 2; i++) {
    hB[i] = h * rt_ODE3_B[2][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2]);
  }

  rtsiSetT(si, tnew);
  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Model step function */
real_T step_ramp_codegen_custom(real_T arg_Error, real_T arg_VcOld, real_T
  arg_PIDOld, real_T *arg_PID)
{
  /* local block i/o variables */
  real_T rtb_Add3;
  real_T rtb_Sum;
  real_T rtb_Add1;

  /* specified return value */
  real_T arg_Vc;
  if (rtmIsMajorTimeStep(step_ramp_codegen_M)) {
    /* set solver stop time */
    rtsiSetSolverStopTime(&step_ramp_codegen_M->solverInfo,
                          ((step_ramp_codegen_M->Timing.clockTick0+1)*
      step_ramp_codegen_M->Timing.stepSize0));
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(step_ramp_codegen_M)) {
    step_ramp_codegen_M->Timing.t[0] = rtsiGetT(&step_ramp_codegen_M->solverInfo);
  }

  /* Derivative: '<Root>/Derivative' */
  {
    real_T t = step_ramp_codegen_M->Timing.t[0];
    real_T timeStampA = step_ramp_codegen_DW.Derivative_RWORK.TimeStampA;
    real_T timeStampB = step_ramp_codegen_DW.Derivative_RWORK.TimeStampB;
    real_T *lastU = &step_ramp_codegen_DW.Derivative_RWORK.LastUAtTimeA;
    if (timeStampA >= t && timeStampB >= t) {
      rtb_Add3 = 0.0;
    } else {
      real_T deltaT;
      real_T lastTime = timeStampA;
      if (timeStampA < timeStampB) {
        if (timeStampB < t) {
          lastTime = timeStampB;
          lastU = &step_ramp_codegen_DW.Derivative_RWORK.LastUAtTimeB;
        }
      } else if (timeStampA >= t) {
        lastTime = timeStampB;
        lastU = &step_ramp_codegen_DW.Derivative_RWORK.LastUAtTimeB;
      }

      deltaT = t - lastTime;
      rtb_Add3 = (arg_Error - *lastU++) / deltaT;
    }
  }

  /* Sum: '<Root>/Add1' incorporates:
   *  Inport: '<Root>/VcOld'
   */
  rtb_Add1 = rtb_Add3 + arg_VcOld;

  /* Sum: '<Root>/Add3' incorporates:
   *  Inport: '<Root>/Error'
   *  Inport: '<Root>/PIDOld'
   */
  rtb_Add3 = arg_Error - arg_PIDOld;

  /* Gain: '<S1>/Filter Coefficient' incorporates:
   *  Gain: '<S1>/Derivative Gain'
   *  Integrator: '<S1>/Filter'
   *  Sum: '<S1>/SumD'
   */
  step_ramp_codegen_B.FilterCoefficient =
    (step_ramp_codegen_P.DerivativeGain_Gain * rtb_Add3 -
     step_ramp_codegen_X.Filter_CSTATE) *
    step_ramp_codegen_P.FilterCoefficient_Gain;

  /* Sum: '<S1>/Sum' incorporates:
   *  Gain: '<S1>/Proportional Gain'
   *  Integrator: '<S1>/Integrator'
   */
  rtb_Sum = (step_ramp_codegen_P.ProportionalGain_Gain * rtb_Add3 +
             step_ramp_codegen_X.Integrator_CSTATE) +
    step_ramp_codegen_B.FilterCoefficient;

  /* Outport: '<Root>/Vc' incorporates:
   *  Sum: '<Root>/Add2'
   */
  arg_Vc = rtb_Add1 + rtb_Sum;

  /* Outport: '<Root>/PID' */
  *arg_PID = rtb_Sum;

  /* Gain: '<S1>/Integral Gain' */
  step_ramp_codegen_B.IntegralGain = step_ramp_codegen_P.IntegralGain_Gain *
    rtb_Add3;
  if (rtmIsMajorTimeStep(step_ramp_codegen_M)) {
    /* Update for Derivative: '<Root>/Derivative' */
    {
      real_T timeStampA = step_ramp_codegen_DW.Derivative_RWORK.TimeStampA;
      real_T timeStampB = step_ramp_codegen_DW.Derivative_RWORK.TimeStampB;
      real_T* lastTime = &step_ramp_codegen_DW.Derivative_RWORK.TimeStampA;
      real_T* lastU = &step_ramp_codegen_DW.Derivative_RWORK.LastUAtTimeA;
      if (timeStampA != rtInf) {
        if (timeStampB == rtInf) {
          lastTime = &step_ramp_codegen_DW.Derivative_RWORK.TimeStampB;
          lastU = &step_ramp_codegen_DW.Derivative_RWORK.LastUAtTimeB;
        } else if (timeStampA >= timeStampB) {
          lastTime = &step_ramp_codegen_DW.Derivative_RWORK.TimeStampB;
          lastU = &step_ramp_codegen_DW.Derivative_RWORK.LastUAtTimeB;
        }
      }

      *lastTime = step_ramp_codegen_M->Timing.t[0];
      *lastU++ = arg_Error;
    }
  }                                    /* end MajorTimeStep */

  if (rtmIsMajorTimeStep(step_ramp_codegen_M)) {
    rt_ertODEUpdateContinuousStates(&step_ramp_codegen_M->solverInfo, arg_Error,
      arg_VcOld, arg_PIDOld, arg_PID);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     */
    ++step_ramp_codegen_M->Timing.clockTick0;
    step_ramp_codegen_M->Timing.t[0] = rtsiGetSolverStopTime
      (&step_ramp_codegen_M->solverInfo);
  }                                    /* end MajorTimeStep */

  return arg_Vc;
}

/* Derivatives for root system: '<Root>' */
void step_ramp_codegen_derivatives(real_T arg_Error, real_T arg_VcOld, real_T
  arg_PIDOld, real_T *arg_PID)
{
  XDot_step_ramp_codegen_T *_rtXdot;
  _rtXdot = ((XDot_step_ramp_codegen_T *) step_ramp_codegen_M->ModelData.derivs);

  /* Derivatives for Integrator: '<S1>/Integrator' */
  _rtXdot->Integrator_CSTATE = step_ramp_codegen_B.IntegralGain;

  /* Derivatives for Integrator: '<S1>/Filter' */
  _rtXdot->Filter_CSTATE = step_ramp_codegen_B.FilterCoefficient;
}

/* Model initialize function */
void step_ramp_codegen_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)step_ramp_codegen_M, 0,
                sizeof(RT_MODEL_step_ramp_codegen_T));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&step_ramp_codegen_M->solverInfo,
                          &step_ramp_codegen_M->Timing.simTimeStep);
    rtsiSetTPtr(&step_ramp_codegen_M->solverInfo, &rtmGetTPtr
                (step_ramp_codegen_M));
    rtsiSetStepSizePtr(&step_ramp_codegen_M->solverInfo,
                       &step_ramp_codegen_M->Timing.stepSize0);
    rtsiSetdXPtr(&step_ramp_codegen_M->solverInfo,
                 &step_ramp_codegen_M->ModelData.derivs);
    rtsiSetContStatesPtr(&step_ramp_codegen_M->solverInfo, (real_T **)
                         &step_ramp_codegen_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&step_ramp_codegen_M->solverInfo,
      &step_ramp_codegen_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&step_ramp_codegen_M->solverInfo, (&rtmGetErrorStatus
      (step_ramp_codegen_M)));
    rtsiSetRTModelPtr(&step_ramp_codegen_M->solverInfo, step_ramp_codegen_M);
  }

  rtsiSetSimTimeStep(&step_ramp_codegen_M->solverInfo, MAJOR_TIME_STEP);
  step_ramp_codegen_M->ModelData.intgData.y =
    step_ramp_codegen_M->ModelData.odeY;
  step_ramp_codegen_M->ModelData.intgData.f[0] =
    step_ramp_codegen_M->ModelData.odeF[0];
  step_ramp_codegen_M->ModelData.intgData.f[1] =
    step_ramp_codegen_M->ModelData.odeF[1];
  step_ramp_codegen_M->ModelData.intgData.f[2] =
    step_ramp_codegen_M->ModelData.odeF[2];
  step_ramp_codegen_M->ModelData.contStates = ((X_step_ramp_codegen_T *)
    &step_ramp_codegen_X);
  rtsiSetSolverData(&step_ramp_codegen_M->solverInfo, (void *)
                    &step_ramp_codegen_M->ModelData.intgData);
  rtsiSetSolverName(&step_ramp_codegen_M->solverInfo,"ode3");
  rtmSetTPtr(step_ramp_codegen_M, &step_ramp_codegen_M->Timing.tArray[0]);
  step_ramp_codegen_M->Timing.stepSize0 = 0.4;

  /* block I/O */
  (void) memset(((void *) &step_ramp_codegen_B), 0,
                sizeof(B_step_ramp_codegen_T));

  /* states (continuous) */
  {
    (void) memset((void *)&step_ramp_codegen_X, 0,
                  sizeof(X_step_ramp_codegen_T));
  }

  /* states (dwork) */
  (void) memset((void *)&step_ramp_codegen_DW, 0,
                sizeof(DW_step_ramp_codegen_T));

  /* InitializeConditions for Derivative: '<Root>/Derivative' */
  step_ramp_codegen_DW.Derivative_RWORK.TimeStampA = rtInf;
  step_ramp_codegen_DW.Derivative_RWORK.TimeStampB = rtInf;

  /* InitializeConditions for Integrator: '<S1>/Integrator' */
  step_ramp_codegen_X.Integrator_CSTATE = step_ramp_codegen_P.Integrator_IC;

  /* InitializeConditions for Integrator: '<S1>/Filter' */
  step_ramp_codegen_X.Filter_CSTATE = step_ramp_codegen_P.Filter_IC;
}

/* Model terminate function */
void step_ramp_codegen_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
