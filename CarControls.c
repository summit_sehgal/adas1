/*
 * CarControls.c
 *
 *  Created on: 2014-03-09
 *      Author: s4sehgal
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/neutrino.h>
#include <sys/netmgr.h>
#include "carControls.h"
#include "CommAvr.h"
#include "controlUtil.h"
#include "CommMatlab.h"
#include <errno.h>
#include "lak.h"
#include "acc.h"
#include "mat.h"
#include <tgmath.h>
#include "stanley_y.h"



con_task_t g_task_con;


void Init_Cont_Data()
{
	memset(&g_task_con, 0, sizeof(con_task_t));
	//matlab inputs
	g_task_con.ESTOP = 1.0;						//Emergency Stop //In // 1: STOP
	g_task_con.lane = 0.0;						//Desired Lane  //In  //0:LEFT 1:RIGHT
	g_task_con.distance = 0.0;					//Desired Cruising Distance //In
	g_task_con.speedR = 0.0;                  	//Set speed from MATLAB GUI
	g_task_con.speedV = 0.0;					//Set speed from MATLAB GUI

	//matlab outputs
	//g_task_con.currentDis = 0;				//Talegate distance between cars //Out
	g_task_con.offset = 0.0;					//Offset from Treadmill corner // Out

	//avr inputs
	g_task_con.adcSt = 0.0;						//Car steer position  //In
	g_task_con.adcTm = 0.0;						//Treadmill Offset ADC //In

	//avr + matlab outputs
	g_task_con.pwmSt = STRAIGHT;				//Steering PWM  //Out
	g_task_con.pwmTm = STATIONARY;						//Treadmill PWM // Out

	//State Variables for ACC
	g_task_con.gapRealL = INITIALGAP;			//We assume at beginning, virtual car has INTIALGAP units lead to real car
	g_task_con.gapRealN = INITIALGAP;			//We assume at beginning, virtual car has INTIALGAP units lead to real car
	g_task_con.posRealL = 0.0;
	g_task_con.posRealN = 0.0;
	g_task_con.speedRealL = 0.0;
	g_task_con.speedRealN = 0.0;
	g_task_con.posVirtL = g_task_con.posRealL + INITIALGAP; //We assume at beginning, virtual car has INTIALGAP units lead to real car
	g_task_con.posVirtN = 0;
	g_task_con.speedVirtL = 0.0;
	g_task_con.speedVirtN = 0.0;

	//State Variables for LAK
	g_task_con.cYaw = 0.0;						//State variable for Car Steering Yaw Angle (Running Counter Var)
	g_task_con.offSet = 0.0;					//State variable for Car Offset from Right (Running Counter Var)

	//Intiatilize PID Controllers for ACC and LAK
	pid_init_acc_test(&g_task_con.pidDataAcc, -50.0, 50.0);
	pid_init_lak_test(&g_task_con.pidDataLak, -1.0, 1.0);
}


void cont_read_avrdata()
{
	//copy from avr communication buffer
	pthread_mutex_lock(&g_task_avr.avr_lock);
	g_task_con.adcSt = g_task_avr.adcStIn;
	g_task_con.adcTm = g_task_avr.adcTmIn;
	pthread_mutex_unlock(&g_task_avr.avr_lock);
}


void cont_read_matdata()
{
	//copy from matlab communication buffer
	pthread_mutex_lock(&g_task_mat.mat_lock);
	g_task_con.distance = g_task_mat.distanceIn;
	g_task_con.lane = g_task_mat.laneIn;
	g_task_con.speedR = g_task_mat.speedInR;
	g_task_con.speedV = g_task_mat.speedInV;
	g_task_con.ESTOP = g_task_mat.ESTOPIN;
	g_task_con.deltaX = g_task_mat.deltaXIn;
	g_task_con.deltaY = g_task_mat.deltaYIn;
	g_task_con.deltaA = g_task_mat.deltaAIn;
	pthread_mutex_unlock(&g_task_mat.mat_lock);
}


void cont_write_avrdata()
{
	//copy to avr communications buffer
	pthread_mutex_lock(&g_task_avr.avr_lock);
	g_task_avr.pwmStOut = g_task_con.pwmSt;
	g_task_avr.pwmTmOut = g_task_con.pwmTm;
	pthread_mutex_unlock(&g_task_avr.avr_lock);
}

void cont_write_matdata()
{
	//copy to matlab communications buffer
	pthread_mutex_lock(&g_task_mat.mat_lock);
	g_task_mat.controlOut = g_task_con.pwmTm;			//From
	g_task_mat.steerOut = g_task_con.pwmSt;
	g_task_mat.curDisOut = g_task_con.gapRealN;
	g_task_mat.offsetOut = g_task_con.offset;
	pthread_mutex_unlock(&g_task_mat.mat_lock);
}

void * SmartCarController(void * args)
{
    int rcvid;                     		 	// process ID of the sender
    MessageT msg;                   		// the message itself

    if ((chid = ChannelCreate (0)) == -1) {
        fprintf (stderr, "couldn't create channel!\n");
        perror (NULL);
        exit (EXIT_FAILURE);
    }

    setupPulseAndTimer ();

	printf("Starting the Car Models\n");

	//usleep(3000000);

	stanley_y_initialize();

	while(1){
		//Block for a pulse
		rcvid = MsgReceive (chid, &msg, sizeof (msg), NULL);

		if (rcvid == 0) {

		//make a local copy of avr communication buffer
		cont_read_avrdata();

		//make a local copy of matlab communication buffer
		cont_read_matdata();

		//call acc (Adaptive cruise control) and pass local data buffer
		//accModel(&g_task_con);

		//call acc (Adaptive cruise control) and pass local data buffer
		//ctcModel(&g_task_con);

		//Matlab imported model
		matModel(&g_task_con);

		//call lak (Lane Assisted Keeping)and pass local data buffer
		//lakModel(&g_task_con);

		//Write the crunched data to avr communication buffer
		cont_write_avrdata();

		//Write the crunched data to matlab communication buffer
		cont_write_matdata();

		//check if Emergency Stop was issued
		//if (g_task_con.ESTOP == 1)
		//	{
		//		printf("ESTOP: Exiting the Car Model\n");
		//		pthread_exit(NULL);
		//   }
		}
		else  //This else belongs to rcvid=0 if
		{
			gotAMessage (rcvid, &msg.msg);
		}
	}
	//You will never get here : )
	printf("Exiting the Car Models\n");
	pthread_exit(NULL);
}

void setupPulseAndTimer (void)
{
    timer_t             timerid;    // timer ID for timer
    struct sigevent     event;      // event to deliver
    struct itimerspec   timer;      // the timer data structure
    int                 coid;       // connection back to ourselves

    // create a connection back to ourselves
    coid = ConnectAttach (0, 0, chid, 0, 0);
    if (coid == -1) {
        fprintf (stderr, "couldn't ConnectAttach to self!\n");
        perror (NULL);
        exit (EXIT_FAILURE);
    }

    // set up the kind of event that we want to deliver -- a pulse
    SIGEV_PULSE_INIT (&event, coid, SIGEV_PULSE_PRIO_INHERIT, CODE_TIMER, 0);

    // create the timer, binding it to the event
    if (timer_create (CLOCK_REALTIME, &event, &timerid) == -1) {
        fprintf (stderr, "couldn't create a timer, errno %d\n", errno);
        perror (NULL);
        exit (EXIT_FAILURE);
    }

    // setup the timer (1s delay, 1s reload)
    timer.it_value.tv_sec = 1;
    timer.it_value.tv_nsec = 0;
    timer.it_interval.tv_sec = 0;
    //timer.it_interval.tv_nsec = 100000000;
    timer.it_interval.tv_nsec = 10000000;

    // and start it!
    timer_settime (timerid, 0, &timer, NULL);
}


/*
 *  gotAPulse
 *
 *  This routine is responsible for handling the fact that a timeout
 *  has occurred.  It runs through the list of clients to see
 *  which client has timed-out, and replies to it with a timed-out
 *  response.
 */

void
gotAPulse (void)
{
    ClientMessageT  msg;
    int             i;

    if (1) {
        time_t  now;

        time (&now);
        printf ("Got a Pulse at %s", ctime (&now));
    }

    // prepare a response message
    msg.messageType = MT_TIMEDOUT;

    // walk down list of clients
    for (i = 0; i < MAX_CLIENT; i++) {

        // is this entry in use?
        if (clients [i].in_use) {

            // is it about to time out?
            if (--clients [i].timeout == 0) {

                // send a reply
                MsgReply (clients [i].rcvid, EOK, &msg, sizeof (msg));

                // entry no longer used
                clients [i].in_use = 0;
            }
        }
    }
}

/*
 *  gotAMessage
 *
 *  This routine is called whenever a message arrives.  We look at the
 *  type of message (either a "wait for data" message, or a "here's some
 *  data" message), and act accordingly.  For simplicity, we'll assume
 *  that there is never any data waiting.  See the text for more discussion
 *  about this.
*/

void
gotAMessage (int rcvid, ClientMessageT *msg)
{
    int i;

    // determine the kind of message that it is
    switch (msg -> messageType) {

    // client wants to wait for data
    case    MT_WAIT_DATA:

        // see if we can find a blank spot in the client table
        for (i = 0; i < MAX_CLIENT; i++) {

            if (!clients [i].in_use) {

                // found one -- mark as in use, save rcvid, set timeout
                clients [i].in_use = 1;
                clients [i].rcvid = rcvid;
                clients [i].timeout = 5;
                return;
            }
        }

        fprintf (stderr, "Table full, message from rcvid %d ignored, "
                         "client blocked\n", rcvid);
        break;

    // client with data
    case    MT_SEND_DATA:

        // see if we can find another client to reply to with this
        // client's data
        for (i = 0; i < MAX_CLIENT; i++) {

            if (clients [i].in_use) {

                // found one -- reuse the incoming message as an
                // outgoing message
                msg -> messageType = MT_OK;

                // reply to BOTH CLIENTS!
                MsgReply (clients [i].rcvid, EOK, msg, sizeof (*msg));
                MsgReply (rcvid, EOK, msg, sizeof (*msg));

                clients [i].in_use = 0;
                return;
            }
        }

        fprintf (stderr, "Table empty, message from rcvid %d ignored, "
                         "client blocked\n", rcvid);
        break;
    }
}
