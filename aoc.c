/*
 * mat.c
 *
 *  Created on: 2014-09-29
 *      Author: Ricky
 */

//DEPRECIATED - NO LONGER IN USE
#ifdef DEPRECIATE

#include "pid.h"
#include "mat.h"
#include "CarControls.h"
#include "controlUtil.h"
#include "xor_test.h"

int aocModel(void * latestData){

	con_task_t * ctcData = (con_task_t *)latestData;
	//printf ("%lf \n", ctcData->adcSt);
	ExtU_xor_test_T in;
	in.In1 = (ctcData->adcSt >= 501);
	in.In2 = (ctcData->adcTm >= 501);

#ifdef DUMMY
	if(ctcData->adcSt >= 500)
		in.In1 =1;
	else
		in.In1 =0;
	if(ctcData->adcTm >= 500)
			in.In2 =1;
		else
			in.In2 =0;
#endif
	//printf ("Log1: %d, Raw1: %lf, log2: %d, Raw2: %lf  \n", in.In1, ctcData->adcSt, in.In2, ctcData->adcTm);

	ExtY_xor_test_T out;

	RT_MODEL_xor_test_T model;
	model.ModelData.inputs = &in;
	model.ModelData.outputs = &out;
	xor_test_step(&model);

	ctcData->pwmSt = out.Out1;
	printf("In1: %d, RawIn1:%lf, In2: %d, RawIn2 :%lf, Out: %d\n", in.In1,ctcData->adcSt, in.In2, ctcData->adcSt, out.Out1);
}

#endif
