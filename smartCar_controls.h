#ifndef SMARTCAR_CONTROLS_H
#define SMARTCAR_CONTROLS_H

/* Parameter Defintions */
#define VEHICLE_WHEELBASE 0.3 //length of vehicle between front and rear axles (m)

/* Variable Definitions */
double KF_Q[2][2];	//Variance matrix for motion model
double KF_R[2][2];	//Variance matrix for measurement model
double KF_P[2][2];	//Covariance matrix of kalman filter estimate

/* Function Prototypes */
double Get_Steering_Angle(double h_err, double ct_err, double v);
double Get_Saturated_Value(double val, double lim);
void Init_Kalman_Filter();
double Get_Heading(double h, double v, double delta, double z, double dt);
double Convert_Crosstrack_To_Heading(double prev_y, double curr_y, double v, double dt);

#endif
