/*
 * lak.c
 *
 *  Created on: 2014-03-10
 *      Author: s4sehgal
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "controlUtil.h"
#include "CarControls.h"
#include "smartCar_controls.h"
#include "pid.h"



int lakModel (void * latestData){
	con_task_t * lakData = (con_task_t *)latestData;

	double hAngle = 0;
	double hRadian = 0;
	double desired = 0;
	double err = 0;
	double stRad = 0;
	double pidL = 0;
	double stDuty = 0;

	//Determine the Lane Position Command
	if (lakData->lane == 0)
		desired = LEFT;
	else
		desired = RIGHT;

	//Simple LPF added to determine Offset from RHS of treadmill
	lakData->offSet = (0.5*lakData->offSet)+(0.5*lakData->adcTm);

	//Simple LPF added to determine Car Yaw
	lakData->cYaw = (0.5*lakData->cYaw)+(0.5*lakData->adcSt);

	//ADC Value at Zero Degree Steer
	hAngle = (lakData->cYaw-480);		//This has to be updated

	//Convert to Radians
	hRadian =  -1*(hAngle/180)*3.14159;	//-1 fixes frame direction

	//Distance between current car position and desired car position
	err = desired - lakData->offSet;

	stRad= Get_Steering_Angle(-hRadian, (err)/800, 1);

	stDuty = ((stRad/3.14159)*180.0*15.375);	//duty in ms

#ifdef PIDLAK

	pidL = pid_update_f(desired, lakData->offset, &lakData->pidDataLak);
	if ((!(lakData->ESTOP)) && (pidL >= 0))
		lakData->pwmSt = STRAIGHT - ((absD(pidL) <= 3.0) ? pidL : 3.0);
	else if ((!(lakData->ESTOP)) && (pidL < 0))
		lakData->pwmSt = STRAIGHT - ((absD(pidL) <= 3.0) ? pidL : -3.0);
	else
		lakData->pwmSt = STRAIGHT;

#endif

#ifdef STANLEY
		if ((!(lakData->ESTOP)) && (stDuty >= 0))
			lakData->pwmSt = STRAIGHT - ((absD(stDuty) <= 3.0) ? stDuty : 3.0);
		else if ((!(accData->ESTOP)) && (stDuty < 0))
			lakData->pwmSt = STRAIGHT - ((absD(stDuty) <= 3.0) ? stDuty : -3.0);
		else
			lakData->pwmSt = STRAIGHT;
#endif
	return 0;
}
