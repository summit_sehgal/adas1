/*
 * xor_test.c
 *
 * Code generation for model "xor_test".
 *
 * Model version              : 1.5
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Thu Nov 06 19:40:12 2014
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARMCortex
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */
#include "xor_test.h"
#include "xor_test_private.h"

/* Model step function */
void xor_test_step(RT_MODEL_xor_test_T *const xor_test_M)
{
  ExtU_xor_test_T *xor_test_U = (ExtU_xor_test_T *) xor_test_M->ModelData.inputs;
  ExtY_xor_test_T *xor_test_Y = (ExtY_xor_test_T *)
    xor_test_M->ModelData.outputs;

  /* Outport: '<Root>/Out1' incorporates:
   *  Inport: '<Root>/In1'
   *  Inport: '<Root>/In2'
   *  Logic: '<Root>/Logical Operator'
   */
  xor_test_Y->Out1 = xor_test_U->In1 ^ xor_test_U->In2;
}

/* Model initialize function */
void xor_test_initialize(RT_MODEL_xor_test_T *const xor_test_M)
{
  UNUSED_PARAMETER(xor_test_M);
}

/* Model terminate function */
void xor_test_terminate(RT_MODEL_xor_test_T * xor_test_M)
{
  /* model code */
  {
    real_T *ptr = (real_T *) xor_test_M->ModelData.inputs;
    rt_FREE(ptr);
  }

  {
    real_T *ptr = (real_T *) xor_test_M->ModelData.outputs;
    rt_FREE(ptr);
  }

  rt_FREE(xor_test_M);
}

/* Model data allocation function */
RT_MODEL_xor_test_T *xor_test(void)
{
  RT_MODEL_xor_test_T *xor_test_M;
  xor_test_M = (RT_MODEL_xor_test_T *) malloc(sizeof(RT_MODEL_xor_test_T));
  if (xor_test_M == NULL) {
    return NULL;
  }

  (void) memset((char *)xor_test_M, 0,
                sizeof(RT_MODEL_xor_test_T));

  /* external inputs */
  {
    ExtU_xor_test_T *xor_test_U = (ExtU_xor_test_T *) malloc(sizeof
      (ExtU_xor_test_T));
    //rt_VALIDATE_MEMORY(xor_test_M,xor_test_U);
    xor_test_M->ModelData.inputs = (((ExtU_xor_test_T *) xor_test_U));
  }

  /* external outputs */
  {
    ExtY_xor_test_T *xor_test_Y = (ExtY_xor_test_T *) malloc(sizeof
      (ExtY_xor_test_T));
    //rt_VALIDATE_MEMORY(xor_test_M,xor_test_Y);
    xor_test_M->ModelData.outputs = (xor_test_Y);
  }

  {
    ExtU_xor_test_T *xor_test_U = (ExtU_xor_test_T *)
      xor_test_M->ModelData.inputs;
    ExtY_xor_test_T *xor_test_Y = (ExtY_xor_test_T *)
      xor_test_M->ModelData.outputs;

    /* external inputs */
    (void) memset((void *)xor_test_U, 0,
                  sizeof(ExtU_xor_test_T));

    /* external outputs */
    xor_test_Y->Out1 = FALSE;
  }

  return xor_test_M;
}
