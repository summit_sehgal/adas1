/*
 * CommMatlab.h
 *
 *  Created on: 2014-03-19
 *      Author: s4sehgal
 */

#ifndef COMMMATLAB_H_
#define COMMMATLAB_H_

#include <pthread.h>
#include <time.h>
#include <stdint.h>

#define MATPORT "/dev/ser3"

/** Type to manage sample task */
typedef struct {
	/** Select Car Drive Lane: 0: LEFT, 1: RIGHT**/
	double laneIn;

	/** Desired Car Cruising Speed Real Car*/
	double speedInR;

	/** Desired Car Cruising Speed Real Car*/
	double speedInV;

	/** Emergency Stop */
	double ESTOPIN;

	/** Desired Safe Cruising Distance */
	double distanceIn;

	/** Control Generated Output, Range 0 to 60 */
	double controlOut;

	/** Control Generated Output, Range 0 to 60 */
	double steerOut;

	/** Actual distance between Cars, Talegate distance */
	double curDisOut;

	/**Offset from Right Edge of the road **/
	double offsetOut;

	/**Offset from Right Edge of the road **/
	double deltaXIn;

	/**Offset from Right Edge of the road **/
	double deltaYIn;

	/**Offset from Right Edge of the road **/
	double deltaAIn;

	/** Controls access to read sensor data. */
	pthread_mutex_t mat_lock;

	/** Handle to manage task. */
	//pthread_t task;

	/** Flag indicating termination. */
	//uint8_t b_term;

	/** Condition to be broadcasted when the first sample has been read. */
	//pthread_cond_t got_data;
} mat_task_t;

/** Handle for MATLAB communication */
extern mat_task_t g_task_mat;

/** Handle for MATLAB communication */
extern mat_task_t g_task_mat;

/** Initialize MATLAB Communication Task */
extern void Init_Comm_Mat(void);

// Parse MATLAB data and atomically write it into g_task_mat structure
extern void bufferParseMat(const char* buffer);

// New buffer parse function with strtok
extern void bufferParseMatN (const char* string);

/** MATLAB Communication Thread Entry Point */
void * Mat_Comm_Entry (void * args);

#endif /* COMMMATLAB_H_ */

