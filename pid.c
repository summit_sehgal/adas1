/*
 * pid.c
 *
 *  Created on: 2014-03-12
 *      Author: s4sehgal
 */
#include <stdlib.h>
#include <stdio.h>
#include "pid.h"


#ifdef test_values
void pid_init_f(pid_f_t * ptr, double min, double max){
	memset(ptr, 0, sizeof(pid_f_t));
	ptr->min = min;
	ptr->max = max;
	ptr->kp = 0.1;
	ptr->ki = 0.3;
	ptr->kd = 0.05;
	ptr->i = 0;
	ptr->e = 0;
}pid_f_t pidData;
#endif


void pid_init_lak(pid_f_t * ptr, double min, double max){
	  memset(ptr, 0, sizeof(pid_f_t));
	  ptr->min = min;
	  ptr->max = max;
	  ptr->kp = 2.5;	//ptr->ki = 0.001;
	  ptr->ki = 0.5;	//ptr->kd = 0.0001;
	  ptr->kd = 0.07;
	  ptr->i = 0;
	  ptr->e = 0;
	}

void pid_init_lak_test(pid_f_t * ptr, double min, double max){
	  memset(ptr, 0, sizeof(pid_f_t));
	  ptr->min = min;
	  ptr->max = max;
	  ptr->kp = 0.05;	//ptr->ki = 0.001;
	  ptr->ki = 0.01;	//ptr->kd = 0.0001;
	  ptr->kd = 0.0001;
	  ptr->i = 0;
	  ptr->e = 0;
	}

void pid_init_acc_test(pid_f_t * ptr, double min, double max){
	  memset(ptr, 0, sizeof(pid_f_t));
	  ptr->min = min;
	  ptr->max = max;
	  ptr->kp = 0.005;	//ptr->ki = 0.001;
	  ptr->ki = 0.001;	//ptr->kd = 0.0001;
	  ptr->kd = 0.0001;
	  ptr->i = 0;
	  ptr->e = 0;
	}

void pid_init_acc(pid_f_t * ptr, double min, double max){
	  memset(ptr, 0, sizeof(pid_f_t));
	  ptr->min = min;
	  ptr->max = max;
	  ptr->kp = 2.5;	//ptr->ki = 0.001;
	  ptr->ki = 0.5;	//ptr->kd = 0.0001;
	  ptr->kd = 0.07;
	  ptr->i = 0;
	  ptr->e = 0;
	}

//sp: desired, pv: current
double pid_update_f(double sp, double pv, pid_f_t * ptr){
  double e;
  double manp;
  double tmpi;
  e = ptr->e;
  ptr->e = sp - pv;
  tmpi = ptr->i + ptr->e;
  //printf("Integral Error: %lf \n", tmpi);

  //bound the integral
  manp = ptr->kp * ptr->e + ptr->ki * tmpi + ptr->kd * ((e - ptr->e));
  if ( (manp < ptr->max) && (manp > ptr->min) ){
    ptr->i = manp;
  } else if ( manp > ptr->max ){
    manp = ptr->max;
  } else if ( manp < ptr->min ){
    manp = ptr->min;
  }
  return manp;
}
