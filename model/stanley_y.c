/*
 * File: stanley_y.c
 *
 * Code generated for Simulink model 'stanley_y'.
 *
 * Model version                  : 1.12
 * Simulink Coder version         : 8.5 (R2013b) 08-Aug-2013
 * C/C++ source code generated on : Wed Dec 03 01:25:50 2014
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#include "stanley_y.h"
#include "stanley_y_private.h"

/* Model step function */
real_T stanley_y_custom(real_T arg_In1)
{
  /* specified return value */
  real_T arg_Out1;

  /* Outport: '<Root>/Out1' incorporates:
   *  Constant: '<Root>/ScalingConst'
   *  Constant: '<Root>/TuningConst'
   *  Inport: '<Root>/In1'
   *  Product: '<Root>/Product'
   *  Product: '<Root>/Product3'
   *  Trigonometry: '<Root>/Trigonometric Function'
   */
  arg_Out1 = atan(0.05 * arg_In1) * 3;
  return arg_Out1;
}

/* Model initialize function */
void stanley_y_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
