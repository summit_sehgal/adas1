/*
 * File: stanley_y_types.h
 *
 * Code generated for Simulink model 'stanley_y'.
 *
 * Model version                  : 1.12
 * Simulink Coder version         : 8.5 (R2013b) 08-Aug-2013
 * C/C++ source code generated on : Wed Dec 03 01:25:50 2014
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_stanley_y_types_h_
#define RTW_HEADER_stanley_y_types_h_
#endif                                 /* RTW_HEADER_stanley_y_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
