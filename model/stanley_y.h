/*
 * File: stanley_y.h
 *
 * Code generated for Simulink model 'stanley_y'.
 *
 * Model version                  : 1.12
 * Simulink Coder version         : 8.5 (R2013b) 08-Aug-2013
 * C/C++ source code generated on : Wed Dec 03 01:25:50 2014
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_stanley_y_h_
#define RTW_HEADER_stanley_y_h_
#ifndef stanley_y_COMMON_INCLUDES_
# define stanley_y_COMMON_INCLUDES_
#include <math.h>
#include "rtwtypes.h"
#endif                                 /* stanley_y_COMMON_INCLUDES_ */

#include "stanley_y_types.h"

/* Macros for accessing real-time model data structure */

/* Model entry point functions */
extern void stanley_y_initialize(void);

/* Customized model step function */
extern real_T stanley_y_custom(real_T arg_In1);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'stanley_y'
 */
#endif                                 /* RTW_HEADER_stanley_y_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
