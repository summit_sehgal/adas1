#include <stdio.h>
#include <math.h>
#include "smartCar_controls.h"

/**
  * Steering Control using Stanley DARPA controller
  * @param h_err heading error (rad)
  * @param ct_err cross track error (m)
  * @param v forward velocity (m/s)
  * @return steering angle (rad)
  */
 double Get_Steering_Angle(double h_err, double ct_err, double v)
 {
	double K = 1;	//this is the controller gain (higher = more sensitive to crosstrack error)
	double G = 1;
/*
	if (fabs(ct_err) < 0.02)
	{
		G = fabs(ct_err);
		K = 2;
	}
	else if (fabs(ct_err) < 0.03)
	{
		K = 2-(((fabs(ct_err)-0.02)/0.01)*1);
		G = (((fabs(ct_err)-0.02)/0.01)*1.48)+0.02;
	}


	G = G+(2000*pow(fabs(h_err),4));
	if (G > 1.5)
	{
		G = 1.5;
	}
*/

	return ((G*h_err) - atan(K*ct_err/v));	//control law
 }

/**
  * General purpose saturator for limiting control inputs
  * @param val the value to be saturated
  * @param lim the saturator limits will be at +/-lim
  * @return the saturated value
  */
 double Get_Saturated_Value(double val, double lim)
 {
	lim = fabs(lim);
	return val > lim ? lim : (val < -1*lim ? -1*lim : val);
 }

 /**
  *	Initializes the Kalman filter matrices
  */
 void Init_Kalman_Filter()
 {
	 //initial estimate covariance matrix
	KF_P[0][0] = 1;	KF_P[0][1] = 0;
	KF_P[1][0] = 0;	KF_P[1][1] = 1;

	//motion model variance matrix
	KF_Q[0][0] = 1; /*0.03;*/	KF_Q[0][1] = 0;
	KF_Q[1][0] = 0;		KF_Q[1][1] = 0.03;

	//measurement model variance matrix
	KF_R[0][0] = 0.03; /*100;*/	KF_R[0][1] = 0;
	KF_R[1][0] = 0;		KF_R[1][1] = 500;
 }

 /**
  * Kalman filter to estimate vehicle heading using Ackermann bicycle model
  * Make sure Init_Kalman_Filter() is called before this function
  * @param h the last heading estimate (rad)
  * @param v forward velocity (m/s)
  * @param delta current steering angle (rad)
  * @param z the current heading measurement (rad)
  * @param dt the time step of the filter loop (s)
  * @return the current heading estimate
  * @see Init_Kalman_Filter
  */
 double Get_Heading(double h, double v, double delta, double z, double dt)
 {
	//motion model update with current control input
	double h_rate_f = (v/VEHICLE_WHEELBASE)*tan(delta);	//new heading rate based on steering angle
	double h_f = h+(h_rate_f*dt);	//new heading based on motion model
	//covariance after motion model update
	double P_f[2][2] = {
		{KF_P[0][0]+(KF_P[0][1]*dt)+(KF_P[1][0]*dt)+(KF_P[1][1]*dt*dt)+KF_Q[0][0],	KF_P[0][1]+(KF_P[1][1]*dt)+KF_Q[0][1]},
		{KF_P[1][0]+(KF_P[1][1]*dt)+KF_Q[1][0],										KF_P[1][1]+KF_Q[1][1]}
	};
	//measurement update
	double v_k[2] = {
		z-h_f,			//heading residual between motion model and measurement
		0-h_rate_f		//heading rate residual, we don't really care about this, so we fake it by assuming measured rate of 0
	};
	double S_k[2][2] = {
		{P_f[0][0]+KF_R[0][0],	P_f[0][1]+KF_R[0][1]},
		{P_f[1][0]+KF_R[1][0],	P_f[1][1]+KF_R[1][1]}
	};
	double S_k_det = (S_k[0][0]*S_k[1][1])-(S_k[0][1]*S_k[1][0]);
	double S_k_inv[2][2] = {
		{S_k[1][1]/S_k_det,		-1*S_k[0][1]/S_k_det},
		{-1*S_k[1][0]/S_k_det,	S_k[0][0]/S_k_det}
	};
	//Kalman filter gain calculation
	double K_k[2][2] = {
		{(P_f[0][0]*S_k_inv[0][0])+(P_f[0][1]*S_k_inv[1][0]),	(P_f[0][0]*S_k_inv[0][1])+(P_f[0][1]*S_k_inv[1][1])},
		{(P_f[1][0]*S_k_inv[0][0])+(P_f[1][1]*S_k_inv[1][0]),	(P_f[1][0]*S_k_inv[0][1])+(P_f[1][1]*S_k_inv[1][1])}
	};

	//Update new estimate covariance
	KF_P[0][0] -= (K_k[0][0]*K_k[0][0]*S_k[0][0])+(K_k[0][0]*K_k[0][1]*S_k[1][0])+(K_k[0][0]*K_k[0][1]*S_k[0][1])+(K_k[0][1]*K_k[0][1]*S_k[1][1]);
	KF_P[0][1] -= (K_k[0][0]*K_k[1][0]*S_k[0][0])+(K_k[0][1]*K_k[1][0]*S_k[1][0])+(K_k[0][0]*K_k[1][1]*S_k[0][1])+(K_k[0][1]*K_k[1][1]*S_k[1][1]);
	KF_P[1][0] -= (K_k[0][0]*K_k[1][0]*S_k[0][0])+(K_k[0][0]*K_k[1][1]*S_k[1][0])+(K_k[0][1]*K_k[1][0]*S_k[0][1])+(K_k[0][1]*K_k[1][1]*S_k[1][1]);
	KF_P[1][1] -= (K_k[1][0]*K_k[1][0]*S_k[0][0])+(K_k[1][0]*K_k[1][1]*S_k[1][0])+(K_k[1][0]*K_k[1][1]*S_k[0][1])+(K_k[1][1]*K_k[1][1]*S_k[1][1]);
	//Update and return new heading estimate
	return h_f+(K_k[0][0]*v_k[0])+(K_k[0][1]*v_k[1]);
 }

 /**
  * Converts the crosstrack measurement into an approximate heading
  * @param prev_y the crosstrack position in the last time step (m)
  * @param curr_y the crosstrack position in the current time step (m)
  * @param v current forward velocity (m/s)
  * @param dt the time step of the loop (s)
  * @return an approximate vehicle heading based on change in y (rad)
  */
 double Convert_Crosstrack_To_Heading(double prev_y, double curr_y, double v, double dt)
 {
		return asin((curr_y-prev_y)/(v*dt));
 }
