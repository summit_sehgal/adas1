/*
 * ctc.h
 *
 *  Created on: 2014-06-07
 *      Author: s4sehgal
 */

#ifndef CTC_H_
#define CTC_H_

//Adaptive Cruise Control Function Signature


#define XOFFSET 80
#define LEFTLOC 51.5

//Car Autopilot Function Signature
int accModelO(void * latestData);
int accModel(void * latestData);
double runACC(double speedRealN, double distance, double gapRealN);
double runCC(double speedRealN, double speedR, double speedRealL);

#endif /* CTC_H_ */
