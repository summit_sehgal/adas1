/*
 * pid.h
 *
 *  Created on: 2014-03-25
 *      Author: s4sehgal
 */

#ifndef PID_H_
#define PID_H_

#define KP_R 0.05
#define KI_R 0.25
#define KD_R 0.01

#define KP_V 0.05
#define KI_V 0.15
#define KD_V 0.01

#define KP_D 0.05
#define KI_D 0.0
#define KD_D 0.01

//#define KP_X 0.0075
//#define KI_X 0.0005 // There seems to a small constant error. This small value seems to fix it. -Shafkat
//#define KD_X 0.04

//actual values
//#define KP_X 0.002//0.0075
//#define KI_X 0.0005 // There seems to a small constant error. This small value seems to fix it. -Shafkat
//#define KD_X 0.02

//for model identification
#define KP_X 0.002//0.0075
#define KI_X 0.0005 // There seems to a small constant error. This small value seems to fix it. -Shafkat
#define KD_X 0.0
// for car modeling purpose
//#define KP_X 0.002//0.0075
//#define KI_X 0.000 // There seems to a small constant error. This small value seems to fix it. -Shafkat
//#define KD_X 0.0

//#define KP_X 0.1
//#define KI_X 0.0005 // There seems to a small constant error. This small value seems to fix it. -Shafkat
//#define KD_X 0.01

//#define KP_X 0.05
//#define KI_X 0.0
//#define KD_X 0.01

#define IntTime = 10;

//#define KP_Y 0.0075
//#define KI_Y 0.0005
//#define KD_Y 0.0005

//#define KP_Y 0.005
//#define KI_Y 0.0
//#define KD_Y 0.001

// THIS WORKS FOR THE GREEN CAR. DONT TOUCH.
//#define KP_Y 0.0020
//#define KI_Y 0.0005
//#define KD_Y 0.0005

//parameters to determine car plant model - from henry
#define KP_Y 0.4
#define KI_Y 0
#define KD_Y 0.05


typedef struct{
	double max 		/*! Max manipulated value */;
	double min 		/*! Miniumum manipulated value */;
	double e 		/*! Error value */;
	double i 		/*! Integrator value */;
	double kp 		/*! Proportional constant */;
	double ki 		/*! Integrator constant */;
	double kd 		/*! Differential constant */;
}pid_f_t;

void pid_init_lak(pid_f_t * ptr, double min, double max);
void pid_init_lak_test(pid_f_t * ptr, double min, double max);
void pid_init_acc(pid_f_t * ptr, double min, double max);
void pid_init_ac_test(pid_f_t * ptr, double min, double max);
extern double pid_update_f(double sp, double pv, pid_f_t * ptr);
double pid_update_fR(double sp,double pv);
double pid_update_fC(double sp,double pv);
double pid_update_fV(double sp,double pv);
double pid_update_fD(double sp,double pv);
double pid_update_fX(double sp,double pv);
double pid_update_fY(double sp,double pv);
double pid_update_fDS(double sp,double pv, double max, double min);


#endif /* PID_H_ */
