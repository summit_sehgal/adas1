/*
 * acc.c
 *
 *  Created on: 2014-03-11
 *      Author: s4sehgal
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#define type1
#include "controlUtil.h"
#include "CarControls.h"
#include "pid.h"

int accModel(void * latestData){
	static int i = 0;
	static double deltaD = 0;
	static double x = 0;
	static double xL = 0;
	static double xLL = 0;
	static double xLLL = 0;
	static double xLLLL = 0;
	con_task_t * accData = (con_task_t *)latestData;

	if (accData->ESTOP == 1)
		{	//Emergency Stop
			accData->speedRealN = 0;
		}
	else
		{																					//Car Cruises
			accData->speedRealN = pid_update_fR(accData->speedR,accData->speedRealL);
			accData->speedVirtN = pid_update_fV(accData->speedV,accData->speedVirtL);
			//	printf("I am in CC \n");
		}

	if ((accData->gapRealN) < (accData->distance))											//ACC Activates
		{
			accData->speedRealN = accData->speedRealN - pid_update_fD(accData->distance,accData->gapRealN);
			//x =  pid_update_fD(accData->distance,accData->gapReal);
			//printf("I am in ACC \n");
		}

	//position(i) = position(i-1) + speed (i) * delta-time
	accData->posRealN = accData->posRealL + accData->speedRealN * DELTAT;					//TO-DO: We can calculate DELTAT from system time
	accData->posVirtN = accData->posVirtL + accData->speedVirtN * DELTAT;

	//Calculate the tail gating gap
	accData->gapRealN = accData->posVirtN - accData->posRealN;

	if (accData->gapRealN <= (1.1*accData->distance))
	{
		if(accData->speedVirtN >= accData->speedR)
			//accData->pwmTm = accData->speedR;
			x = accData->speedR;
		else
			//accData->pwmTm = accData->speedVirtN;
			x = accData->speedVirtN;
	}
	else
		//accData->pwmTm = accData->speedRealN;
		x = accData->speedRealN;

	//Safe saturation limits
	if (x >= 60)
		x = 60;
	if (x <= 0)
		x = 0;

	//Delta Distance traveled in last time step
	deltaD = (accData->gapRealN) - (accData->gapRealL);
	//accData->pwmTm = accData->speedVirtN - (10*deltaD);

	//Update rolling buffers
	accData->speedRealL = accData->speedRealN;
	accData->speedVirtL = accData->speedVirtN;
	accData->posRealL = accData->posRealN;							// i becomes i-1
	accData->posVirtL = accData->posVirtN;							// i becomes i-1
	accData->gapRealL = accData->gapRealN;

	//Fill LPF window
	xLLLL = xLLL;
	xLLL = xLL;
	xLL = xL;
	xL = x;

	//LPF
	accData->pwmTm = (x + xL + xLL + xLLL + xLLLL)/5.0;

	//printout scaledown
	//if (i >= 4)
	//{
		//printf("%lf, %lf, %lf, %lf, %lf, %lf, %lf\n",accData->speedRealN, accData->speedR, accData->speedVirtN, accData->speedV, accData->gapRealN, deltaD, accData->pwmTm);
		//i = 0;
	//}
	//i++;
}


double pid_update_fR(double sp,double pv)
{
	double absErr = sp-pv;
	static double absErrL = 0;
	static double intErr = 0;
	static double difErr = 0;
	double pidE = 0;

	intErr = intErr + (absErr * DELTAT);
	difErr = (absErr - absErrL)/ DELTAT;

	pidE = KP_R * absErr + KI_R * intErr + KD_R * difErr;

	//Integrator anti-windup
	if (absD(intErr) >= 500)
		intErr = 500;

	absErrL = absErr;
	return pidE;
}

double pid_update_fV(double sp,double pv)
{
	double absErr = sp-pv;
	static double absErrL = 0;
	static double intErr = 0;
	static double difErr = 0;
	double pidE = 0;

	intErr = intErr + (absErr * DELTAT);
	difErr = (absErr - absErrL)/ DELTAT;

	pidE = KP_V * absErr + KI_V * intErr + KD_V * difErr;
	absErrL = absErr;
	return pidE;
}

double pid_update_fDS(double sp,double pv, double max, double min)
{
	double absErr = sp-pv;
	static double absErrL = 0;
	static double intErr = 0;
	static double difErr = 0;
	double pidE = 0;

	intErr = intErr + (absErr * DELTAT);
	difErr = (absErr - absErrL)/ DELTAT;

	pidE = KP_D * absErr + KI_D * intErr + KD_D * difErr;

	if ( (pidE <= max) && (pidE >= min) ){
	    return pidE;
	  } else if ( pidE > max ){
	    return max;
	  } else if ( pidE < min ){
	    return min;
	  }
	absErrL = absErr;
}

double pid_update_fD(double sp,double pv)
{
	double absErr = sp-pv;
	static double absErrL = 0;
	static double intErr = 0;
	static double difErr = 0;
	double pidE = 0;

	intErr = intErr + (absErr * DELTAT);
	difErr = (absErr - absErrL)/ DELTAT;

	pidE = KP_D * absErr + KI_D * intErr + KD_D * difErr;

	//Integrator anti-windup
	if (absD(intErr) >= 500)
		intErr = 500;

	absErrL = absErr;
	return pidE;
}








// OLD ACC MODEL
#ifdef OLDACC
int accModelO(void * latestData){
	con_task_t * accData = (con_task_t *)latestData;
	//pid_f_t pidDataAcc;
	//pid_init_acc(&pidDataAcc, -7.0, 60.0);
	double pidE;
    //double d1 = accData->distance;
	double time=1;              //To Do: Time Elapsed based on System Time
	static double lastSpeed=7.0;

	accData->d0+= lastSpeed*time;

	accData->d1+= accData->speed*time;

	double currentDis =((accData->d1)-(accData->d0));      //state variable
	pidE = pid_update_f(accData->distance, currentDis, &accData->pidDataAcc);

	//printf("%lf \n", pidE);
#ifdef type1
	if ((!(accData->ESTOP)) && (pidE >= 0))
		accData->pwmTm = lastSpeed + ((absD(pidE) <= 50.0)? pidE : 50.0);  //Treadmill PWM Duty 0 to 60
	else if ((!(accData->ESTOP)) && (pidE < 0))
		accData->pwmTm = lastSpeed + ((absD(pidE) <= 50.0)? pidE : -50.0);
	else
		accData->pwmTm = 0;     //Emergency Stop
#endif

#ifdef type2
	if(accData->ESTOP == 1)
	{
		//printf("Got to Stop Now \n");
		accData->pwmTm = 0;
	}
	else
		accData->pwmTm = lastSpeed + pidE;
#endif

	lastSpeed = accData->pwmTm;

	if (accData->d0>10000.0 && accData->d1>10000.0){
		accData->d0 -= 1000.0;
		accData->d1 -= 1000.0;
	}

	//graph_s=controller_output;
		//accData->pwmTm;
	//graph_d=actual_distance;
	return 0;
}

#endif
