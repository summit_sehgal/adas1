/*
 * File: pid_calculation.c
 *
 * Code generated for Simulink model 'pid_calculation'.
 *
 * Model version                  : 1.3
 * Simulink Coder version         : 8.5 (R2013b) 08-Aug-2013
 * C/C++ source code generated on : Fri Dec 19 15:49:25 2014
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#include "pid_calculation.h"
#include "pid_calculation_private.h"

/* Real-time model */
RT_MODEL_pid_calculation_T pid_calculation_M_;
RT_MODEL_pid_calculation_T *const pid_calculation_M = &pid_calculation_M_;

/* Model step function */
real_T pid_calculation_custom(real_T arg_Reference, real_T arg_Position, real_T
  arg_OldIErr, real_T arg_Step, real_T arg_LastDistError, real_T *arg_IError,
  real_T *arg_DistError)
{
  real_T rtb_Subtract1;
  real_T rtb_Product;
  real_T rtb_Add2;

  /* specified return value */
  real_T arg_Out1;

  /* Sum: '<Root>/Subtract' incorporates:
   *  Inport: '<Root>/Position'
   *  Inport: '<Root>/Reference'
   */
  rtb_Subtract1 = arg_Reference - arg_Position;

  /* Product: '<Root>/Product' incorporates:
   *  Constant: '<Root>/Kp'
   */
  rtb_Product = pid_calculation_P.Kp_Value * rtb_Subtract1;

  /* Sum: '<Root>/Add2' incorporates:
   *  Inport: '<Root>/OldIErr'
   *  Inport: '<Root>/Step'
   *  Product: '<Root>/Product2'
   */
  rtb_Add2 = rtb_Subtract1 * arg_Step + arg_OldIErr;

  /* Sum: '<Root>/Subtract1' incorporates:
   *  Inport: '<Root>/LastDistError'
   */
  rtb_Subtract1 -= arg_LastDistError;

  /* Outport: '<Root>/Out1' incorporates:
   *  Constant: '<Root>/Kd'
   *  Constant: '<Root>/Ki'
   *  Inport: '<Root>/Step'
   *  Product: '<Root>/Divide'
   *  Product: '<Root>/Product1'
   *  Product: '<Root>/Product3'
   *  Sum: '<Root>/Add'
   *  Sum: '<Root>/Add1'
   */
  arg_Out1 = (rtb_Subtract1 / arg_Step * pid_calculation_P.Kd_Value +
              pid_calculation_P.Ki_Value * rtb_Add2) + rtb_Product;
  /* Outport: '<Root>/IError' */
  *arg_IError = rtb_Add2;

  /* Outport: '<Root>/DistError' */
  *arg_DistError = rtb_Subtract1;
  return arg_Out1;
}

/* Model initialize function */
void pid_calculation_initialize(void)
{
  /* Registration code */

  /* initialize error status */
  rtmSetErrorStatus(pid_calculation_M, (NULL));
}

/* Model terminate function */
void pid_calculation_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
