/*
 * File: pid_calculation_private.h
 *
 * Code generated for Simulink model 'pid_calculation'.
 *
 * Model version                  : 1.3
 * Simulink Coder version         : 8.5 (R2013b) 08-Aug-2013
 * C/C++ source code generated on : Fri Dec 19 15:49:25 2014
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_pid_calculation_private_h_
#define RTW_HEADER_pid_calculation_private_h_
#include "rtwtypes.h"
#ifndef __RTWTYPES_H__
#error This file requires rtwtypes.h to be included
#else
#ifdef TMWTYPES_PREVIOUSLY_INCLUDED
#error This file requires rtwtypes.h to be included before tmwtypes.h
#endif                                 /* TMWTYPES_PREVIOUSLY_INCLUDED */
#endif                                 /* __RTWTYPES_H__ */
#endif                                 /* RTW_HEADER_pid_calculation_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
