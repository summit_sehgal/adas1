/*
 * File: pid_calculation_types.h
 *
 * Code generated for Simulink model 'pid_calculation'.
 *
 * Model version                  : 1.3
 * Simulink Coder version         : 8.5 (R2013b) 08-Aug-2013
 * C/C++ source code generated on : Fri Dec 19 15:49:25 2014
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_pid_calculation_types_h_
#define RTW_HEADER_pid_calculation_types_h_
#include "rtwtypes.h"

/* Parameters (auto storage) */
typedef struct P_pid_calculation_T_ P_pid_calculation_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_pid_calculation_T RT_MODEL_pid_calculation_T;

#endif                                 /* RTW_HEADER_pid_calculation_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
