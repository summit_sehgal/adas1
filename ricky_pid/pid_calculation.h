/*
 * File: pid_calculation.h
 *
 * Code generated for Simulink model 'pid_calculation'.
 *
 * Model version                  : 1.3
 * Simulink Coder version         : 8.5 (R2013b) 08-Aug-2013
 * C/C++ source code generated on : Fri Dec 19 15:49:25 2014
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_pid_calculation_h_
#define RTW_HEADER_pid_calculation_h_
#ifndef pid_calculation_COMMON_INCLUDES_
# define pid_calculation_COMMON_INCLUDES_
#include <stddef.h>
#include "rtwtypes.h"
#endif                                 /* pid_calculation_COMMON_INCLUDES_ */

#include "pid_calculation_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Parameters (auto storage) */
struct P_pid_calculation_T_ {
  real_T Kp_Value;                     /* Expression: 1
                                        * Referenced by: '<Root>/Kp'
                                        */
  real_T Ki_Value;                     /* Expression: 1
                                        * Referenced by: '<Root>/Ki'
                                        */
  real_T Kd_Value;                     /* Expression: 1
                                        * Referenced by: '<Root>/Kd'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_pid_calculation_T {
  const char_T * volatile errorStatus;
};

/* Block parameters (auto storage) */
extern P_pid_calculation_T pid_calculation_P;

/* Model entry point functions */
extern void pid_calculation_initialize(void);
extern void pid_calculation_terminate(void);

/* Customized model step function */
extern real_T pid_calculation_custom(real_T arg_Reference, real_T arg_Position,
  real_T arg_OldIErr, real_T arg_Step, real_T arg_LastDistError, real_T
  *arg_IError, real_T *arg_DistError);

/* Real-time Model object */
extern RT_MODEL_pid_calculation_T *const pid_calculation_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'pid_calculation'
 */
#endif                                 /* RTW_HEADER_pid_calculation_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
