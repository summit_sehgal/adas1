/*
 * File: pid_calculation_data.c
 *
 * Code generated for Simulink model 'pid_calculation'.
 *
 * Model version                  : 1.3
 * Simulink Coder version         : 8.5 (R2013b) 08-Aug-2013
 * C/C++ source code generated on : Fri Dec 19 15:49:25 2014
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#include "pid_calculation.h"
#include "pid_calculation_private.h"

/* Block parameters (auto storage) */
P_pid_calculation_T pid_calculation_P = {
  0.75,                                 /* Expression: 1
                                        * Referenced by: '<Root>/Kp'
                                        */
  0.25,                                 /* Expression: 1
                                        * Referenced by: '<Root>/Ki'
                                        */
  0.2                                  /* Expression: 1
                                        * Referenced by: '<Root>/Kd'
                                        */
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
