/*
 * lak.h
 *
 *  Created on: 2014-03-25
 *      Author: s4sehgal
 */

#ifndef LAK_H_
#define LAK_H_

//Lane Assistance Keeping Function Signature
int lakModel (void * latestData);


#endif /* LAK_H_ */
