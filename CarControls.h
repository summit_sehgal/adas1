/*
 * CarControls.h
 *
 *  Created on: 2014-03-19
 *      Author: s4sehgal
 */

#ifndef CARCONTROLS_H_
#define CARCONTROLS_H_

#include <pthread.h>
#include <time.h>
#include <stdint.h>
#include <signal.h>
#include "pid.h"
#include <sys/siginfo.h>
#include <sys/neutrino.h>


/** Type to manage sample task */
typedef struct {
	//avr side variables
	double adcSt;				//in
	double adcTm;				//in

	//shared variables (avr(for control) and matlab(for display and monitoring)
	double pwmTm;				//output to both AVR and MATLAB
	double pwmSt;				//output to both AVR and MATLAB

	//matlab side variables
	double distance;			//in
	double lane;				//in
	double speedR;				//in
	double speedV;				//in
	double ESTOP;				//in
	double deltaX;				//in
	double deltaY;				//in
	double deltaA;				//in
	double offset;				//out

	//State Variables
	// ACC Real Car State Variables
	double speedRealL;
	double speedRealN;
	double posRealL;
	double posRealN;

	//ACC Virtual Car State Variables
	double speedVirtL;
	double speedVirtN;
	double posVirtL;
	double posVirtN;

	//ACC other state variables
	double gapRealL;
	double gapRealN;

	//LAK State Variables
	double cYaw;				//State Variable1 for LAK
	double offSet;				//State Variable2 for LAK
	pid_f_t pidDataAcc;			//Contains State Variables in structure
	pid_f_t pidDataLak;			//Contains State Variables in structure
} con_task_t;


/**
 * This function copies the buffers over from the avr communication task
 * @see avr_task_t, g_task_avr
 */
extern void cont_read_avrdata(void);

/**
 * This function copies the buffers over from the matlab communication task
 * @see mat_task_t g_task_mat
 */
extern void cont_read_matdata(void);

/**
 * This function copies the buffers to the avr communication task
 * @see avr_task_t, g_task_avr
 */
extern void cont_write_avrdata(void);

/**
 * This function copies the buffers to the matlab communication task
 * @see mat_task_t g_task_mat
 */
extern void cont_write_matdata(void);


/** Global pointer Car Controls task. */
extern con_task_t g_task_con;

/** Initialize Car Controllers Task */
extern void Init_Cont_Data();

/** Smart Car Controllers Thread Entry Point */
void * SmartCarController(void * args);


// message send definitions

// messages
#define MT_WAIT_DATA        2       // message from client
#define MT_SEND_DATA        3       // message from client

// pulses
#define CODE_TIMER          1       // pulse from timer

// message reply definitions
#define MT_OK               0       // message to client
#define MT_TIMEDOUT         1       // message to client

// message structure
typedef struct
{
    int messageType;                // contains both message to and from client
    int messageData;                // optional data, depending upon message
} ClientMessageT;

typedef union
{
    ClientMessageT  msg;            // a message can be either from a client, or
    struct _pulse   pulse;          // a pulse
} MessageT;

// client table
#define MAX_CLIENT 16               // maximum number of simultaneous clients

struct
{
    int in_use;                     // is this client entry in use?
    int rcvid;                      // receive ID of client
    int timeout;                    // timeout left for client
}   clients [MAX_CLIENT];           // client table

int     chid;                       // channel ID (global)


// forward prototypes
static  void setupPulseAndTimer (void);
static  void gotAPulse (void);
static  void gotAMessage (int rcvid, ClientMessageT *msg);

#endif /* CARCONTROLS_H_ */




