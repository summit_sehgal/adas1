/*
 * acc.h
 *
 *  Created on: 2014-03-26
 *      Author: s4sehgal
 */

#ifndef ACC_H_
#define ACC_H_

//Adaptive Cruise Control Function Signature
int accModelO(void * latestData);
int accModel(void * latestData);
double runACC(double speedRealN, double distance, double gapRealN);
double runCC(double speedRealN, double speedR, double speedRealL);

#endif /* ACC_H_ */
