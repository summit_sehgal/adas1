/*
 * mat.h
 *
 *  Created on: 2014-11-27
 *      Author: Ricky
 */

#ifndef MAT_H_
#define MAT_H_


#define XOFFSET 80
#define LEFTLOC 51.5

//Function Signature for handler for MATLAB imported controllers
int matModel(void * latestData);

#endif /* MAT_H_ */
