/*
 * CommMatlab.c
 *
 *  Created on: 2014-03-10
 *      Author: s4sehgal
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/neutrino.h>
#include <sys/select.h>
#include <pthread.h>
#include <termios.h>
#include <fcntl.h>
#include "CommMatlab.h"
#include "CommonCom.h"
#include "CarControls.h"
#include <time.h>
#include <tgmath.h>

mat_task_t g_task_mat; //Init Globals

/*
 * Initializes Comm Thread
 */
void Init_Comm_Mat()
{
	//Set memory for matlab task variable
	memset(&g_task_mat, 0, sizeof(mat_task_t));
	//mutex init for matlab comm thread
	pthread_mutex_init(&g_task_mat.mat_lock,NULL);
}


/*
 * Point of entry into communications thread
 */
void * Mat_Comm_Entry (void * args)
{
	int serMat;
	int sz = 0;
	int mRead = 0;
	//char rxBufferMat[UART_BUF_LEN];
	char * rxBufferMat = (char*)malloc(UART_BUF_LEN);
	//char txBufferMat[UART_BUF_LEN];
	char * txBufferMat = (char*)malloc(UART_BUF_LEN);

	//Serial Stuff: Start
	serMat = open(MATPORT, O_RDWR|O_NONBLOCK);
	if (serMat == -1){
		printf("Failed to open Matlab serial port that links to MATLAB Host");
	}
	printf("Matlab Serial Channel Opened\n");
	//delay(200);
	fd_set rfd;
	FD_ZERO(&rfd);
	FD_SET(serMat, &rfd);
	SerialSetup_Stream(&serMat,38400,8,0,1);
	tcdrain(serMat);
	tcflush(serMat,TCIOFLUSH);
	//Serial Stuff: Ends

	while (1) {
			char c;

			/* piece line together of measurements */
			mRead = read(serMat, &c, 1);
			//printf("could not read from Matlab interface \n");
			if (mRead > 0){
				if(c == '@') {
				sz = 0;
				} else if(c == '!') { /* got complete line, update measurements */
					rxBufferMat[sz++] = '\0';
					bufferParseMatN(rxBufferMat);
					//TO MATLAB
					int num = sprintf(txBufferMat,"$%lf,%lf,%lf,%lf*", g_task_mat.controlOut, g_task_mat.steerOut, g_task_mat.curDisOut, g_task_mat.offsetOut);
					//int num = sprintf(txBufferMat,"$%lf,%lf,%lf,%lf*\n", g_task_mat.controlOut, g_task_mat.steerOut, g_task_mat.curDisOut, g_task_mat.offsetOut);
					write(serMat, txBufferMat, num);
					//write(1, txBufferMat, num);
					sz = 0; /* reset buffer */
					} else {
					if(sz == UART_BUF_LEN)
						printf("Matlab Input buffer exhausted \n");
						rxBufferMat[sz++] = c;
					}
				}
			else;
			}
			close(serMat);
			fflush(stdout);
			free(rxBufferMat);
			free(txBufferMat);
			rxBufferMat = NULL;
			txBufferMat = NULL;
			pthread_mutex_destroy(&g_task_mat.mat_lock);
			return NULL;
	}

// Here is a sample packet from AVR should look like:$
void bufferParseMat(const char* buffer) {
    double ln, spr, spv, dis, st;
    //FROM MATLAB
    //printf("%s \n", buffer);
	if(sscanf(buffer, "%lf,%lf,%lf,%lf,%lf", &ln, &spr, &spv, &dis, &st) != 5) {
		perror("Matlab Buffer Read Error");
	}
	pthread_mutex_lock(&g_task_mat.mat_lock);
	g_task_mat.laneIn = ln;
	g_task_mat.speedInR = spr;
	g_task_mat.speedInV = spv;
	g_task_mat.distanceIn = dis;
	g_task_mat.ESTOPIN = st;
	pthread_mutex_unlock(&g_task_mat.mat_lock);
	printf("LaneIn: %lf, SpeedInR: %lf, SpeedInV: %lf, DisIn: %lf, ESTOP: %lf\n", g_task_mat.laneIn, g_task_mat.speedInR,g_task_mat.speedInV, g_task_mat.distanceIn, g_task_mat.ESTOPIN);
	//printf("SpeedIn: %lf \n", g_task_mat.speedIn);
}

void bufferParseMatN (const char* string) {
	char * tmpString = (char*)malloc((strlen(string)+1));
	memcpy(tmpString, string,(strlen(string)+1));
	char* ln; char* spr; char* spv; char* dis; char* st; char* delX; char* delY; char* delA;
	//float ad1, ad2;

	pthread_mutex_lock(&g_task_mat.mat_lock);
	ln = strtok (tmpString,",");
	g_task_mat.laneIn = atof(ln);

	spr = strtok (NULL,",");
	g_task_mat.speedInR = atof(spr);

	spv = strtok (NULL,",");
	g_task_mat.speedInV = atof(spv);

	dis = strtok (NULL,",");
	g_task_mat.distanceIn = atof(dis);

	st = strtok (NULL, ",");
	g_task_mat.ESTOPIN = atof(st);

	delX = strtok (NULL,",");
	g_task_mat.deltaXIn = atof(delX);

	delY = strtok (NULL,",");
	g_task_mat.deltaYIn = atof(delY);

	delA = strtok (NULL,"\0");
	g_task_mat.deltaAIn = atof(delA);
	pthread_mutex_unlock(&g_task_mat.mat_lock);

	//printf("LaneIn: %lf, SpeedInR: %lf, SpeedInV: %lf, DisIn: %lf, ESTOP: %lf, delX: %lf, delY: %lf, delA: %lf\n", g_task_mat.laneIn, g_task_mat.speedInR,g_task_mat.speedInV, g_task_mat.distanceIn, g_task_mat.ESTOPIN, g_task_mat.deltaXIn, g_task_mat.deltaYIn, g_task_mat.deltaAIn);

	free(tmpString);
	tmpString = NULL;
}



