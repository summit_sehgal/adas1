/*
 * CommAvr.c
 *
 *  Created on: 2014-03-09
 *      Author: s4sehgal
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <string.h>
#include <sys/neutrino.h>
#include <sys/select.h>
#include <pthread.h>
#include "CommAvr.h"
#include "CarControls.h"
#include "CommonCom.h"
#include <time.h>
#include <tgmath.h>

avr_task_t g_task_avr; /* init globals */

/*
 * Initializes Comm Thread
 */
void Init_Comm_Avr()
{
	//Set memory for avr task variable
	memset(&g_task_avr, 0, sizeof(avr_task_t));
	//mutex init for avr comm thread
	pthread_mutex_init(&g_task_avr.avr_lock,NULL);
}



/*
 * Point of entry into communications thread
 */
void * Avr_Comm_Entry (void * args)
{
	int serAvr;
	int sz = 0;
	int aRead = 0;
	//char rxBufferAvr[UART_BUF_LEN];
	char * rxBufferAvr = (char*)malloc(UART_BUF_LEN);
	//char txBufferAvr[UART_BUF_LEN];
	char * txBufferAvr = (char*)malloc(UART_BUF_LEN);


	//Serial Stuff: Start
	serAvr = open(AVRPORT, O_RDWR|O_NONBLOCK);
	if (serAvr == -1){
		printf("Failed to open ADC PWM port that links to AVR");
	}
	printf("AVR TTL Channel Opened\n");
	//delay(200);
	fd_set rfd;
	FD_ZERO(&rfd);
	FD_SET(serAvr, &rfd);
	SerialSetup_Stream(&serAvr,38400,8,0,1);
	tcdrain(serAvr);
	tcflush(serAvr,TCIOFLUSH);
	//Serial Stuff: Ends


	while (1) {
		char c;

			aRead = read(serAvr, &c, 1);
				//printf("could not read from AVR interface \n");
			if (aRead > 0){
				if(c == '$') {
					sz = 0;
				} else if(c == '*') { /* got complete line, update measurements */
					//rxBufferAvr[sz++] = '\0';
					bufferParseAvrN(rxBufferAvr);
					//TO AVR
					int num = sprintf(txBufferAvr,"$%lf,%lf*", g_task_avr.pwmStOut, g_task_avr.pwmTmOut);
					//int num = sprintf(txBufferAvr,"$%lf,%lf* \n", g_task_avr.pwmStOut, g_task_avr.pwmTmOut);
					write(serAvr, txBufferAvr, num);
					//write(1, txBufferAvr, num);
					sz = 0; /* reset buffer */
				} else {
					if(sz == UART_BUF_LEN)
						printf("AVR Input buffer exhausted \n");
					rxBufferAvr[sz++] = c;
				}
			}
			else;
		}
		close(serAvr);
		fflush(stdout);
		free(rxBufferAvr);
		free(txBufferAvr);
		rxBufferAvr = NULL;
		txBufferAvr = NULL;
		pthread_mutex_destroy(&g_task_avr.avr_lock);
		return NULL;
	}

// Here is a sample packet from AVR should look like: $adc1,adc2*
void bufferParseAvr(const char* buffer) {
	double ad1, ad2;
	//FROM AVR
	if(sscanf(buffer, "%lf,%lf", &ad1, &ad2) != 2) {
		perror("AVR Buffer Read Error");
	}
	pthread_mutex_lock(&g_task_avr.avr_lock);
	g_task_avr.adcStIn = ad1;
	g_task_avr.adcTmIn = ad2;
	pthread_mutex_unlock(&g_task_avr.avr_lock);
	//printf("adcStIn: %lf, adcTmIn: %lf\n", g_task_avr.adcStIn, g_task_avr.adcTmIn); /* comment this */
}

void bufferParseAvrN (const char* string) {
	char * tmpString = (char*)malloc((strlen(string)+1));
	memcpy(tmpString, string,(strlen(string)+1));
	char* ad1S; char* ad2S;
	//float ad1, ad2;

	pthread_mutex_lock(&g_task_avr.avr_lock);
	ad1S = strtok (tmpString,",");
	g_task_avr.adcStIn = atof(ad1S);
	ad2S = strtok (NULL, "\0");
	g_task_avr.adcTmIn = atof(ad2S);
	pthread_mutex_unlock(&g_task_avr.avr_lock);
	//printf("adcStIn: %lf, adcTmIn: %lf\n", g_task_avr.adcStIn, g_task_avr.adcTmIn); /* comment this */

	free(tmpString);
	tmpString = NULL;
	}
