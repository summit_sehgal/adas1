/*
 * xor_test.c
 *
 * Code generation for model "xor_test".
 *
 * Model version              : 1.1
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Wed Oct 22 20:26:10 2014
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */
#include "xor_test.h"
#include "xor_test_private.h"

/* Model step function */
void xor_test_step(RT_MODEL_xor_test_T *const xor_test_M)
{
  ExtU_xor_test_T *xor_test_U = (ExtU_xor_test_T *) xor_test_M->ModelData.inputs;
  ExtY_xor_test_T *xor_test_Y = (ExtY_xor_test_T *)
    xor_test_M->ModelData.outputs;

  /* Outport: '<Root>/Out1' incorporates:
   *  Inport: '<Root>/In1'
   *  Inport: '<Root>/In2'
   *  Logic: '<Root>/Logical Operator'
   */
  xor_test_Y->Out1 = xor_test_U->In1 ^ xor_test_U->In2;

  /* Matfile logging */
  rt_UpdateTXYLogVars(xor_test_M->rtwLogInfo, (&xor_test_M->Timing.taskTime0));

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.2s, 0.0s] */
    if ((rtmGetTFinal(xor_test_M)!=-1) &&
        !((rtmGetTFinal(xor_test_M)-xor_test_M->Timing.taskTime0) >
          xor_test_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(xor_test_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++xor_test_M->Timing.clockTick0)) {
    ++xor_test_M->Timing.clockTickH0;
  }

  xor_test_M->Timing.taskTime0 = xor_test_M->Timing.clockTick0 *
    xor_test_M->Timing.stepSize0 + xor_test_M->Timing.clockTickH0 *
    xor_test_M->Timing.stepSize0 * 4294967296.0;
}

/* Model initialize function */
void xor_test_initialize(RT_MODEL_xor_test_T *const xor_test_M)
{
  UNUSED_PARAMETER(xor_test_M);
}

/* Model terminate function */
void xor_test_terminate(RT_MODEL_xor_test_T * xor_test_M)
{
  /* model code */
  {
    real_T *ptr = (real_T *) xor_test_M->ModelData.inputs;
    rt_FREE(ptr);
  }

  {
    real_T *ptr = (real_T *) xor_test_M->ModelData.outputs;
    rt_FREE(ptr);
  }

  {
    void *xptr = (void *) rtliGetLogXSignalPtrs(xor_test_M->rtwLogInfo);
    void *yptr = (void *) rtliGetLogYSignalPtrs(xor_test_M->rtwLogInfo);
    rt_FREE(xptr);
    rt_FREE(yptr);
  }

  rt_FREE(xor_test_M->rtwLogInfo);
  rt_FREE(xor_test_M);
}

/* Model data allocation function */
RT_MODEL_xor_test_T *xor_test(void)
{
  RT_MODEL_xor_test_T *xor_test_M;
  xor_test_M = (RT_MODEL_xor_test_T *) malloc(sizeof(RT_MODEL_xor_test_T));
  if (xor_test_M == NULL) {
    return NULL;
  }

  (void) memset((char *)xor_test_M, 0,
                sizeof(RT_MODEL_xor_test_T));

  /* Setup for data logging */
  {
    RTWLogInfo *rt_DataLoggingInfo = (RTWLogInfo *) malloc(sizeof(RTWLogInfo));
    rt_VALIDATE_MEMORY(xor_test_M,rt_DataLoggingInfo);
    xor_test_M->rtwLogInfo = rt_DataLoggingInfo;
  }

  /* Setup for data logging */
  {
    rtliSetLogXSignalInfo(xor_test_M->rtwLogInfo, (NULL));
    rtliSetLogXSignalPtrs(xor_test_M->rtwLogInfo, (NULL));

    /*
     * Set pointers to the data and signal info for each output
     */
    {
      void **rt_LoggedOutputSignalPtrs = (void **)malloc(1*sizeof(void*));
      rt_VALIDATE_MEMORY(xor_test_M,rt_LoggedOutputSignalPtrs);
      rtliSetLogYSignalPtrs(xor_test_M->rtwLogInfo, ((LogSignalPtrsType)
        rt_LoggedOutputSignalPtrs));
    }

    {
    }
  }

  /* external inputs */
  {
    ExtU_xor_test_T *xor_test_U = (ExtU_xor_test_T *) malloc(sizeof
      (ExtU_xor_test_T));
    rt_VALIDATE_MEMORY(xor_test_M,xor_test_U);
    xor_test_M->ModelData.inputs = (((ExtU_xor_test_T *) xor_test_U));
  }

  /* external outputs */
  {
    ExtY_xor_test_T *xor_test_Y = (ExtY_xor_test_T *) malloc(sizeof
      (ExtY_xor_test_T));
    rt_VALIDATE_MEMORY(xor_test_M,xor_test_Y);
    xor_test_M->ModelData.outputs = (xor_test_Y);
  }

  {
    ExtU_xor_test_T *xor_test_U = (ExtU_xor_test_T *)
      xor_test_M->ModelData.inputs;
    ExtY_xor_test_T *xor_test_Y = (ExtY_xor_test_T *)
      xor_test_M->ModelData.outputs;

    /* initialize non-finites */
    rt_InitInfAndNaN(sizeof(real_T));
    rtmSetTFinal(xor_test_M, 10.0);
    xor_test_M->Timing.stepSize0 = 0.2;

    /* Setup for data logging */
    {
      rtliSetLogT(xor_test_M->rtwLogInfo, "tout");
      rtliSetLogX(xor_test_M->rtwLogInfo, "");
      rtliSetLogXFinal(xor_test_M->rtwLogInfo, "");
      rtliSetSigLog(xor_test_M->rtwLogInfo, "");
      rtliSetLogVarNameModifier(xor_test_M->rtwLogInfo, "rt_");
      rtliSetLogFormat(xor_test_M->rtwLogInfo, 0);
      rtliSetLogMaxRows(xor_test_M->rtwLogInfo, 1000);
      rtliSetLogDecimation(xor_test_M->rtwLogInfo, 1);

      /*
       * Set pointers to the data and signal info for each output
       */
      {
        ((void **) rtliGetLogYSignalPtrs(xor_test_M->rtwLogInfo))[0] =
          &xor_test_Y->Out1;
      }

      {
        static int_T rt_LoggedOutputWidths[] = {
          1
        };

        static int_T rt_LoggedOutputNumDimensions[] = {
          1
        };

        static int_T rt_LoggedOutputDimensions[] = {
          1
        };

        static boolean_T rt_LoggedOutputIsVarDims[] = {
          0
        };

        static void* rt_LoggedCurrentSignalDimensions[] = {
          (NULL)
        };

        static int_T rt_LoggedCurrentSignalDimensionsSize[] = {
          4
        };

        static BuiltInDTypeId rt_LoggedOutputDataTypeIds[] = {
          SS_BOOLEAN
        };

        static int_T rt_LoggedOutputComplexSignals[] = {
          0
        };

        static const char_T *rt_LoggedOutputLabels[] = {
          "" };

        static const char_T *rt_LoggedOutputBlockNames[] = {
          "xor_test/Out1" };

        static RTWLogDataTypeConvert rt_RTWLogDataTypeConvert[] = {
          { 0, SS_BOOLEAN, SS_BOOLEAN, 0, 0, 0, 1.0, 0, 0.0 }
        };

        static RTWLogSignalInfo rt_LoggedOutputSignalInfo[] = {
          {
            1,
            rt_LoggedOutputWidths,
            rt_LoggedOutputNumDimensions,
            rt_LoggedOutputDimensions,
            rt_LoggedOutputIsVarDims,
            rt_LoggedCurrentSignalDimensions,
            rt_LoggedCurrentSignalDimensionsSize,
            rt_LoggedOutputDataTypeIds,
            rt_LoggedOutputComplexSignals,
            (NULL),

            { rt_LoggedOutputLabels },
            (NULL),
            (NULL),
            (NULL),

            { rt_LoggedOutputBlockNames },

            { (NULL) },
            (NULL),
            rt_RTWLogDataTypeConvert
          }
        };

        rtliSetLogYSignalInfo(xor_test_M->rtwLogInfo, rt_LoggedOutputSignalInfo);

        /* set currSigDims field */
        rt_LoggedCurrentSignalDimensions[0] = &rt_LoggedOutputWidths[0];
      }

      rtliSetLogY(xor_test_M->rtwLogInfo, "yout");
    }

    xor_test_M->Timing.stepSize = (0.2);

    /* external inputs */
    (void) memset((void *)xor_test_U, 0,
                  sizeof(ExtU_xor_test_T));

    /* external outputs */
    xor_test_Y->Out1 = FALSE;
  }

  return xor_test_M;
}
