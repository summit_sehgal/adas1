/*
 * xor_test_types.h
 *
 * Code generation for model "xor_test".
 *
 * Model version              : 1.1
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Wed Oct 22 20:26:10 2014
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */
#ifndef RTW_HEADER_xor_test_types_h_
#define RTW_HEADER_xor_test_types_h_

/* Forward declaration for rtModel */
typedef struct tag_RTM_xor_test_T RT_MODEL_xor_test_T;

#endif                                 /* RTW_HEADER_xor_test_types_h_ */
