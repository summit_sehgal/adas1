/*
 * CommAvr.h
 *
 *  Created on: 2014-03-19
 *      Author: s4sehgal
 */

#ifndef COMMAVR_H_
#define COMMAVR_H_

#include <pthread.h>
#include <time.h>
#include <stdint.h>

#define AVRPORT "/dev/ser2"


/** Type to manage sample task */
typedef struct {
	/** Treadmill Position ADC In */
	double adcTmIn;

	/** Car Chassis ADC In */
	double adcStIn;

	/** PWM Treadmill Out to AVR */
	double pwmTmOut;

	/** PWM Steering Out to AVR */
	double pwmStOut;

	/** Controls access to read avr data. */
	pthread_mutex_t avr_lock;

	/** Handle to manage task. */
	//pthread_t task;

	/** Flag indicating termination. */
	//uint8_t b_term;

	/** Condition to be broadcasted when the first sample has been read. */
	//pthread_cond_t got_data;
} avr_task_t;


/** Handle for AVR communication */
extern avr_task_t g_task_avr;

/** Initialize AVR Communication Task */
extern void Init_Comm_Avr(void);

// Parse AVR data and atomically write it into g_task_avr structure
extern void bufferParseAvr(const char* buffer);

/** AVR Communication Thread Entry Point */
void * Avr_Comm_Entry (void * args);

#endif /* COMMAVR_H_ */
