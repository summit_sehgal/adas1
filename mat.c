/*
 * mat.c
 *
 *  Created on: 2014-11-27
 *      Author: Ricky
 */

#include "pid.h"
#include "pid_calculation.h"
#include "step_ramp_codegen.h"
#include "mat.h"
#include "CarControls.h"
#include "controlUtil.h"
#include "stanley_y.h" //replace with model header

#define MOTOR_BIAS 15

double log2distX(double logical);
double log2distY(double logical);

#ifdef ARCTAN
int matModel(void * latestData){
	static int i = 0;
	static int j = 0;
	con_task_t * ctcData = (con_task_t *)latestData;
	//printf("adcStIn: %lf, adcTmIn: %lf\n", ctcData->adcSt, ctcData->adcTm);
	real_T in = (double)25-log2distX(ctcData->adcTm);
	//real_T in2 = log2distX(ctcData->adcSt);
	real_T out;
	if (i < 100)
	{
		i++;
		out = 14.86; //signature
	}
	else
	{
		out = stanley_y_custom(in);
		//out = 0.8;

		if (out < 0)
			out = 0;
		if (out > 1.5)
			out = 1.5;
		out += 0.7;
		out += MOTOR_BIAS;
	}
	ctcData->pwmTm = out;
	printf("In1: %lf, RawIn1:%lf, Out: %lf\n", in,ctcData->adcTm, ctcData->pwmTm);
	//j++;
	//if (j == 10)
	//printf("%lf, %lf, %lf\n", in, ctcData->adcTm, ctcData->pwmTm);
	//j%=10;
}
#endif

int matModel(void * latestData){
	static real_T Ierr = 0;
	static real_T Derr = 0;
	static int i = 0;
	static double y = 0;
	con_task_t * ctcData = (con_task_t *)latestData;
	real_T pos = log2distX(ctcData->adcTm);




	y = pid_update_fY(22, pos);			//y: motor

		if (y >= 2.5) 						//Safe saturation limits, 14.86+Y Or 14.86-0, where 14.86 is idle
			y = 2.5;
		if (y <= 0)
			y = 0;

		ctcData->pwmTm = 15.7 + y;
		//ctcData->pwmTm = 15.7;
		if ( i%10 == 0)
		{
			printf("Out: %lf, Pos: %lf \n", ctcData->pwmTm, pos);
		}

		if (i <= 100)
		{
			ctcData->pwmTm = 14.86;
		}
		i++;
}



double pid_update_fY(double sp,double pv){
		double absErr = sp-pv;
		static double absErrL = 0;
		static double intErr = 0;
		static double difErr = 0;
		double pidE = 0;

		intErr = intErr + (absErr * DELTAT);
		difErr = (absErr - absErrL)/ DELTAT;

		pidE = KP_Y * absErr + KI_Y * intErr + KD_Y * difErr;

		//Integrator anti-windup
		if (absD(intErr) >= 50)
			intErr = 50;

		absErrL = absErr;

		return pidE;
}


#ifdef RICKY
	real_T out;
		if (i < 100)
		{
			if (i == 0)
				pid_calculation_initialize();
			i++;
			out = 14.86; //signature
		}
		else
		{

#ifdef ConstTreadPID
			//constant treadmill speed
			real_T newIerr = 0;
			real_T newDerr = 0;
			out = pid_calculation_custom(25 /*temp placeholder*/, pos, Ierr, 0.1, Derr, &newIerr,
				  &newDerr);
			Ierr = newIerr;
			Derr = newDerr;

			double treadmilldc = 0.8; //0.8 = 26 on threadmill
			out += treadmilldc;
#endif

#ifdef ErrTreadPID
			//treadmill speed calculated through error
			static int oldOut = 0;
			static int oldPID = 0;
			//25 is temporary target
			out = step_ramp_codegen_custom(25-pos, oldOut, oldPID, &oldPID);
			oldOut = out;
#endif


			if (out < 0)
				out = 0;
			if (out > 1.5)
				out = 1.5;
			out += MOTOR_BIAS;
			//printf("In1: %lf, RawIn1:%lf, Out: %lf\n", 25-pos,ctcData->adcTm, out);
		}
		ctcData->pwmTm = out;

}


//short range sensor
double log2distX(double logical)
{
	if (logical > 500 || logical < 150) // camera is out of range
		return 25;
	double P1 = -6.84;
	double P2 = 483;
	double dist = logical;
	dist -= P2;
	dist /= P1;
	return dist;
}

//long range sensor
double log2distY(double logical)
{
	double P1 = -0.00000148;
	double P2 = 0.00202;
	double P3 = -0.9792;
	double P4 = 192.8;
	double dist = 0;
	dist += (P1*logical*logical*logical);
	dist += (P2*logical*logical);
	dist += (P3*logical);
	dist += P4;
	return dist;
}
#endif


double log2distX(double logical)
{
	if (logical > 500 || logical < 150) // camera is out of range
		return 25;
	double P1 = -6.84;
	double P2 = 483;
	double dist = logical;
	dist -= P2;
	dist /= P1;
	return dist;
}


