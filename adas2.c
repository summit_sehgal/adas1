#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <pthread.h>
#include "CommAvr.h"
#include "CommMatlab.h"
#include "CarControls.h"
#include "pid.h"

int main(int argc, char *argv[])
{
	Init_Comm_Avr();
	//Init_Comm_Mat();
	Init_Cont_Data();
	pthread_t avr_thread, mat_thread, con_thread;

	printf("Creating AVR Communication Thread\n");
	pthread_create(&avr_thread, NULL, Avr_Comm_Entry , NULL);

	//printf("Creating MATLAB Communication Thread\n");
	//pthread_create(&mat_thread, NULL, Mat_Comm_Entry, NULL);

	printf("Creating Car Controller Thread (LAK and ACC)\n");
	pthread_create(&con_thread, NULL, SmartCarController, NULL);

	pthread_join(avr_thread,NULL);
	pthread_join(mat_thread,NULL);
	pthread_join(con_thread,NULL);
	return EXIT_SUCCESS;
}
