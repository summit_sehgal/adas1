/*
 * aoc.h
 *
 *  Created on: 2014-09-29
 *      Author: Ricky
 */

//DEPRECIATED

#ifndef AOC_H_
#define AOC_H_

//Add One Controller Function Signature


#define XOFFSET 80
#define LEFTLOC 51.5

//Car Autopilot Function Signature
int accModelO(void * latestData);
int accModel(void * latestData);
double runACC(double speedRealN, double distance, double gapRealN);
double runCC(double speedRealN, double speedR, double speedRealL);

#endif /* AOC_H_ */
