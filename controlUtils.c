#include <stdio.h>
#include <stdlib.h>
#include <sys/neutrino.h>
#include <sys/netmgr.h>
#include <sys/syspage.h>
#include <time.h>
#include <assert.h>

int SetContPulse()
{
	int chid,pulse_id;
	timer_t timer_id;
	struct sigevent event;
	struct itimerspec timer;
	struct _clockperiod clkper;

	struct timespec res;
	float cpu_freq;
	time_t start;
//**************************Pulse Setup for Fixed dT******************************************************

	if ( clock_getres( CLOCK_REALTIME, &res) == -1 ) {
		  perror( "clock get resolution" );
		  return EXIT_FAILURE;
		}
	printf( "Resolution is %ld micro seconds.\n",
		  res.tv_nsec / 1000 );

	// --- set tick to 10us ...needs permissions, lowest allowed is 10us, interrupt load
	//default is 1000us for >40MHz Processors
	//This changes for EVERYTHING (adjust delay calls!). Make sure Adaptive partitioning stuff is before or after?...see help on ClockPeriod
	clkper.nsec       = 100000;
	//clkper.nsec       = 1000000000;
	clkper.nsec       = 1000000; //TODO: 1ms, low overhead on serial IMU stream
	//clkper.nsec       = 1000000000; //TODO: 1ms, low overhead on serial IMU stream
	clkper.fract      = 0;
	ClockPeriod ( CLOCK_REALTIME, &clkper, NULL, 0  );

	if ( clock_getres( CLOCK_REALTIME, &res) == -1 ) {
		  perror( "clock get resolution" );
		  return EXIT_FAILURE;
		}
		printf( "Resolution is %ld micro seconds.\n",
			  res.tv_nsec / 1000 );

	// --- Get CPU frequency in order to do precise time calculation
	cpu_freq =  SYSPAGE_ENTRY( qtime )->cycles_per_sec;

	// --- Set priority to max so we don't get disrupted but anything
	// --- else then interrupts
	{
		struct sched_param param;
		int ret;
		param.sched_priority = sched_get_priority_max( SCHED_RR );
		ret = sched_setscheduler( 0, SCHED_RR, &param);
		assert ( ret != -1 );
	}

	// --- Create channel to receive timer event
	chid = ChannelCreate( 0 );
	assert ( chid != -1 );

	// --- setup timer and timer event
	event.sigev_notify            = SIGEV_PULSE;
	event.sigev_coid              = ConnectAttach ( ND_LOCAL_NODE, 0, chid, 0, 0 );
	event.sigev_priority          = getprio(0);
	event.sigev_code              = 1023;
	event.sigev_value.sival_ptr  = (void*)pulse_id;

	assert ( event.sigev_coid != -1 );

	if ( timer_create(  CLOCK_REALTIME, &event, &timer_id ) == -1 )
	{
		perror ( "can't create timer" );
		exit( EXIT_FAILURE );
	}

	timer.it_value.tv_sec        = 0;//first tick after 2 seconds?
	//timer.it_value.tv_nsec       = 10000000;//20000000;//13333333;//SEBASTIAN Test 500000;//
	timer.it_value.tv_nsec       = 100000000;//20000000;//13333333;//SEBASTIAN Test 500000;//
	timer.it_interval.tv_sec     = 0;
	//timer.it_interval.tv_nsec    = 10000000;//20000000;//13333333;//SEBASTIAN Test 500000;// //75 Hz. 1 less than 76 Hz (keep different from IMU, easier to compare and see if working)
	timer.it_interval.tv_nsec    = 100000000;
	//with a 1ms tick, 13.33 ms request will be anywhere from 13.3 to 13.39
	//Reduced tick to 1 us<-Too much interrupt overload.


	// --- start timer
	if ( timer_settime( timer_id, 0, &timer, NULL ) == -1 )
	{
		perror("Can't start timer \n");
		exit( EXIT_FAILURE );

	// --- keep track of time
	start = time(NULL);

//********************End Pulse Setup for Fixed dT************************************************************
return chid;
}
}

double absD(double n)
{
    if (n < 0.0)
        n = -n;
    return n;
}
