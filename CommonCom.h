/*
 * CommonCom.h
 *
 *  Created on: 2014-03-24
 *      Author: s4sehgal
 */

#ifndef COMMONCOM_H_
#define COMMONCOM_H_
#define UART_BUF_LEN 64
//Serial Stream Setup Function
void SerialSetup_Stream(	int* fd,
							int baudrate,
							int dataBits,
							int parity,
							int stopBits );

#endif /* COMMONCOM_H_ */
