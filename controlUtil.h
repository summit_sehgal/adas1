/*
 * controlUtil.h
 *
 *  Created on: 2014-03-24
 *      Author: s4sehgal
 */

#ifndef CONTROLUTIL_H_
#define CONTROLUTIL_H_

//Signature for Pulse setting QNX function
int SetContPulse();

double absD(double n);

#define RIGHT 420
#define CENTRE RIGHT+95
#define LEFT RIGHT+190

//** Uncomment following line for simple PID Controller **//
#define PIDLAK
#define STRAIGHT 14.86
#define STATIONARY 14.86
#define INITIALGAP 1000.0
#define DELTAT 0.01

//** Uncomment following line for Stanley Controller **//
//#define STANLEY

#endif /* CONTROLUTIL_H_ */
